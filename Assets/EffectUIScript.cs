﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EffectUIScript : MonoBehaviour {

	public static EffectUIScript instance;

	public GameObject sightOverlay;
	public Image getHitOverlay;

	void Awake() {
		if (instance == null) {
			instance = this;
		} else {
			Destroy (this);
		}
	}

	void Start(){
		getHitOverlay.gameObject.SetActive (false);
		sightOverlay.SetActive (false);
	}

	public void SetActiveSight(bool active){
		sightOverlay.SetActive (active);
	}

	public void ShowGetHitOverlay(){
		StopCoroutine (GetHit ());
		StartCoroutine (GetHit ());
	}

	IEnumerator GetHit(){
		getHitOverlay.gameObject.SetActive (true);
		Color startColor = getHitOverlay.color;
		startColor.a = 1;
		while (startColor.a > 0) {
			startColor.a -= 0.3f*Time.deltaTime;
			getHitOverlay.color = startColor;
			yield return null;
		}
		getHitOverlay.gameObject.SetActive (false);
	}
}
