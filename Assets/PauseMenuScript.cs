﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PauseMenuScript : MonoBehaviour {
	public Text challengeText;

	// Use this for initialization
	void Start(){
		SetChallengeDescription ();
		gameObject.SetActive (false);
	}

	void SetChallengeDescription(){
		challengeText.text = "CHALLENGE\n";
		foreach (Challenge ch in LevelManager.levelManagerInstance.challenges) {
			challengeText.text += ch.GetDescription () +"\n";
		}
	}
}
