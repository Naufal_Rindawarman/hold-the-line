﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMovement : MonoBehaviour {

	// Use this for initialization
	void Start () {
		StartCoroutine (Shake (2f, 5f, 50));
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	IEnumerator ShakeVertical(float duration, float amplitude, int rate){
		float t0 = duration;
		float rotX;
		while (duration > 0) {
			rotX = transform.rotation.eulerAngles.x;
			rotX += duration*amplitude*Mathf.Sin ((t0-duration)*rate);
			transform.rotation = Quaternion.Euler (rotX, transform.rotation.eulerAngles.y, transform.rotation.eulerAngles.z);
			duration -= Time.deltaTime;
			yield return null;
		}
	}

	IEnumerator ShakeHorizontal(float duration, float amplitude, int rate){
		float t0 = duration;
		float rotY;
		while (duration > 0) {
			rotY = transform.rotation.eulerAngles.y;
			rotY += duration*amplitude*Mathf.Sin ((t0-duration)*rate);
			transform.rotation = Quaternion.Euler (transform.rotation.eulerAngles.x, rotY, transform.rotation.eulerAngles.z);
			duration -= Time.deltaTime;
			yield return null;
		}
	}

	IEnumerator Shake(float duration, float amplitude, int rate){
		float t0 = duration;
		float rotX, rotY;
		while (duration > 0) {
			rotY = transform.rotation.eulerAngles.y;
			rotY += duration*amplitude*Mathf.Sin ((t0-duration)*rate);
			rotX = transform.rotation.eulerAngles.x;
			rotX += duration*amplitude*Mathf.Sin ((t0-duration)*rate);
			transform.rotation = Quaternion.Euler (rotX, rotY, transform.rotation.eulerAngles.z);
			duration -= Time.deltaTime;
			yield return null;
		}
	}
}
