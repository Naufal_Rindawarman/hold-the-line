﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CanvasScript : MonoBehaviour {

	public Canvas canvas;
	public static CanvasScript instance;

	// Use this for initialization
	void Awake () {
		if (instance == null) {
			instance = this;
			canvas = GetComponent<Canvas> ();
		} else if (instance != this){
			Destroy (this);
		}
	}
}
