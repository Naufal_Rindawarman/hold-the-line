using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class LevelManager : MonoBehaviour {

	public static LevelManager levelManagerInstance;
	public int score = 0;

	public float multiplier = 1;

	public static List<WarMachine> units;

	public int enemyAlive = 0;
	public int enemyReserve = 0;

	public int shootTaken = 0;
	public int shootHit = 0;
	public int hitTaken = 0;

	public Challenge[] challenges;

	public bool isGamePlaying = true;

	// Use this for initialization
	void Awake(){
		if (levelManagerInstance == null){
			levelManagerInstance = this;

			if (units == null) {
				units = new List<WarMachine>();
			} else {
				units.Clear ();
			}


		} else if (levelManagerInstance != this) {
			Destroy(this);
		}
	}

	void Start(){
		multiplier = 1f;
		Cursor.visible = false;
		/*
		foreach (Challenge ch in challenges) {
			StartCoroutine (ch.StartChallenge ());
		}
		*/
	}


	public static void CreateLevelManager(){
		if (levelManagerInstance == null) {
			//new GameObject ("LevelManager").AddComponent<LevelManager> ();
		}
	}

	public void restartLevel(){
		SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
	}

	public void NextLevel(){
		int nextIndex = SceneManager.GetActiveScene ().buildIndex + 1;
		//Scene scene = SceneManager.sceneCountInBuildSettings (currentLevelIdx + 1);
		if (nextIndex >= SceneManager.sceneCountInBuildSettings) {
			ExitLevel ();
			return;
		}
		GameManager.Instance.selectedLevel = nextIndex;
		SceneManager.LoadScene (nextIndex);

	}

	public void PauseGame(){
		Time.timeScale = 0f;
		IngameGUIScript.instance.gameObject.SetActive (false);
		Cursor.visible = true;
		Debug.Log("Paused");
	}

	public void ResumeGame(){
		Time.timeScale = 1f;
		IngameGUIScript.instance.gameObject.SetActive (true);
		Cursor.visible = false;
	}

	public void ExitLevel(){
		GameManager.Instance.firstMenu = FirstMenu.LevelSelect;
		SceneManager.LoadScene (0);
	}

	public void ExitToWeaponSelect(){
		GameManager.Instance.firstMenu = FirstMenu.GunSelect;
		SceneManager.LoadScene (0);
	}

	public void ExitGame(){
		Application.Quit();
	}

	public void AddScore(int i){
		score += (int)(i*multiplier);
		IngameGUIScript.instance.scoreTextUI.text = "Score: " + score;
	}

	public void AddScoreWithoutMultiplier(int i){
		score += i;
		IngameGUIScript.instance.scoreTextUI.text = "Score: " + score;
	}

	public void AddScore(){
		score += 10;
		IngameGUIScript.instance.scoreTextUI.text = "Score: " + score;
	}

	public void HitTarget(){
		multiplier += .1f;
	}

	public void MissTarget(){
		multiplier = 1;
	}

	public void DestroyTarget(){
		multiplier += 1f;
		CheckWinCondition ();
	}

	public float GetAccuracy(){
		if (shootTaken == 0) {
			return 0;
		}
		return (float)(shootHit / shootTaken);
	}

	public void CheckWinCondition(){
		if (LevelManager.levelManagerInstance.enemyAlive <= 0 && LevelManager.levelManagerInstance.enemyReserve <= 0) {
			Debug.Log ("Win");
			EndGame ();
			GameMenuManager.instance.endMissionPanel.SetActive (true);
			EndMissionScript.endMissionScriptInstance.Victory ();
		}
	}

	public void Lose(){
		EndGame ();
		GameMenuManager.instance.endMissionPanel.SetActive (true);
		EndMissionScript.endMissionScriptInstance.Defeat ();
	}

	void EndGame(){
		foreach (Challenge ch in challenges) {
			//ch.StopChallenge ();
			score += ch.TryGetReward ();
		}
		score += (int)GetAccuracy () * 100;
		score += Player.instance.GetDefenseGunScript ().getCurrentHitPoint () * 100 / Player.instance.GetDefenseGunScript ().data.maxHealth;
		score += CallSupportAction.instance.GetSupportPoint ();
		GameManager.Instance.GOLD += (int)(score/100);
		Cursor.visible = true;
	}


}
