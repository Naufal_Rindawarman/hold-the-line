﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraInstance : MonoBehaviour {

	public static Camera cameraInstance;

	// Use this for initialization
	void Awake () {
		cameraInstance = Camera.main;

		if (cameraInstance == null) {
			cameraInstance = new GameObject ("MainCamera").AddComponent<Camera> ();
			cameraInstance.tag = "MainCamera";
		}			

		if (cameraInstance.GetComponent<AudioListener> () == null) {
			cameraInstance.gameObject.AddComponent<AudioListener> ();
		}
	}
}
