﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class VoiceManager : MonoBehaviour {

	public AudioClip[] callAlliedTankClip;
	public AudioClip[] callAlliedAircraftClip;
	public AudioClip[] callArtilleryClip;


	public static VoiceManager instance = null;
	private AudioSource source;

	void Awake (){
		if (instance == null) {
			instance = this;
		}

		else if (instance != this) {
			Destroy (this);
		}
	}

	void Start(){
		source = GetComponent<AudioSource> ();
	}

	public void PlayCallAlliedTankClip(){
		PlayClip (callAlliedTankClip);
	}

	public void PlayCallAlliedArtilleryClip(){
		PlayClip (callArtilleryClip);
	}

	public void PlayCallAlliedAircraftClip(){
		PlayClip (callAlliedAircraftClip);
	}

	void PlayClip(AudioClip[] clips){
		if (source.isPlaying) {
			return;
		}
		source.Stop ();
		source.clip = clips[Random.Range(0,clips.Length)];
		source.Play ();
	}

}
