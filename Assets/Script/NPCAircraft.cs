﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class NPCAircraft : NPC {

	public Aircraft aircraftScript;

	private float turnRate = 0.001f;
	private float defaultAltitude;

	public Vector3 zoomTarget;

	public string state;

	private float shootDuration = 0f;


	void Start(){
		defaultAltitude = transform.position.y;
		if (waypoint != null) {
			dispatchWaypoint (waypoint);
		}
		if (target != null) {
			issueTarget (target);
		}
		StartCoroutine (checkTarget ());
	}

	void Update(){
		if (aircraftScript.isAlive ()) {
			if (myOrder != null) {
				myOrder ();
			}
		} else {
			stopFire ();
		}
	}

	IEnumerator checkTarget(){
		while (aircraftScript.alive) {
			if (LevelManager.units != null) {
				for (int i = 0; i < LevelManager.units.Count; i++) {
					WarMachine wm = LevelManager.units [i];
					if (wm.tag == "Dead") {
						target = null;
						//ResumeWayPoint ();
						continue;
					}

					//If met different team
					if (wm.transform.Equals (target) || wm.transform.Equals(transform)) {
						continue;
					}
					if (wm.team != aircraftScript.team) {						
						Debug.Log ("enemy");
			
						if (target == null) {
							issueTarget (wm.transform);
						} else if (target.tag == "GroundVehicle" && wm.tag == "Aircraft"){
							issueTarget (wm.transform);
						} else if (target.tag == "Aircraft" && wm.tag == "Aircraft" && Vector3.Distance(transform.position, wm.transform.position)/Vector3.Distance(transform.position,target.position) < 0.7f){
							issueTarget (wm.transform);
						}
					

					}
				}

			}
			if (target == null) {
				yield return new WaitForSeconds (0.1f);
			} else {
				yield return new WaitForSeconds (1f);
			}

		}
	}

	public void dispatchWaypoint(Transform targetWaypoint){
		myOrder -= goToPosition ;
		waypoint = targetWaypoint;
		destination = waypoint.position + randomXZ();
		waypointScript = targetWaypoint.GetComponent<Waypoint> ();
		turnRate = 0.004f;
		myOrder += goToPosition ;
	}

	public void ResumeWayPoint(){
		myOrder -= goToPosition ;
		myOrder += goToPosition ;
	}

	private void goToPosition(){		
		headTo();
		if (projectedDistance (destination) < 10) {
			Transform nextWaypoint = waypointScript.getRandomNextWaypoint ();
			if (nextWaypoint != null) {
				dispatchWaypoint (nextWaypoint);
			} else {
				loiter ();
			}
		}
	}


	private void loiter(){
		destination = waypoint.position + randomXZ();
		turnRate = 0.0005f;
	}

	public void issueTarget(Transform newTarget){
		target = newTarget;
		destination = target.position;
		myOrder -= goToPosition ;
		if (newTarget.tag == "GroundVehicle") {
			myOrder += approachTarget ;
			state = "Approach";
		} else if (newTarget.tag == "Aircraft") {
			myOrder += pursuit ;
		}
		turnRate = 0.05f;
		myOrder += calculateDestination ;

	}

	void calculateDestination(){
		destinationDistance = projectedDistance(destination);
	}

	//Same altitude
	void headTo(){
		Vector3 levelDestination = new Vector3 (destination.x, transform.position.y, destination.z) - transform.position;

		Quaternion targetRotation = Quaternion.LookRotation (Vector3.Lerp(transform.forward, levelDestination, turnRate*Time.deltaTime));
		transform.rotation = targetRotation;
		//float angle = -targetRotation.eulerAngles.y;
		//float roll = Mathf.Lerp(transform.rotation.eulerAngles.z, angle, 1);
		//transform.rotation = Quaternion.Euler(transform.rotation.eulerAngles.x, transform.rotation.eulerAngles.y, roll);
	}

	//Go to Y
	void goTo(){
		Vector3 levelDestination = new Vector3 (destination.x, transform.position.y, destination.z) - transform.position;
		Quaternion targetRotation = Quaternion.LookRotation (Vector3.Lerp(transform.forward, destination-transform.position, turnRate*Time.deltaTime));
		transform.rotation = targetRotation;
		//float angle = Vector3.Angle (transform.forward, levelDestination);
		//float roll = Mathf.Lerp(transform.rotation.eulerAngles.z, angle*5, 0.05f);
		//transform.rotation = Quaternion.Euler(transform.rotation.eulerAngles.x, transform.rotation.eulerAngles.y, roll);
	}

	void goTo(Vector3 targetVector){
		Vector3 levelDestination = new Vector3 (targetVector.x, transform.position.y, targetVector.z) - transform.position;
		Quaternion targetRotation = Quaternion.LookRotation (Vector3.Lerp(transform.forward, targetVector-transform.position, turnRate*Time.deltaTime));
		transform.rotation = targetRotation;
		float angle = Vector3.Angle (transform.forward, levelDestination);
		float roll = Mathf.Lerp(transform.rotation.eulerAngles.z, angle*5, 0.05f);
		transform.rotation = Quaternion.Euler(transform.rotation.eulerAngles.x, transform.rotation.eulerAngles.y, roll);
	}

	void Level(){
		Vector3 dest = transform.forward*800;
		dest.y = defaultAltitude;
		goTo (dest);
	}

	private void pursuit(){
		goTo (target.position);
		if (Vector3.Distance(transform.position, target.position) < 250) {
			myOrder -= pursuit ;
			myOrder += shootDown;
			turnRate = 0.05f;
			shootDuration = Random.Range(3,7);
			openFire();
		}
	}

	private void shootDown(){
		goTo (target.position);
		shootDuration -= Time.deltaTime;
		if (Vector3.Distance(transform.position, target.position) < 50 || shootDuration < 0f) {
			//destination = transform.forward + new Vector3 (50, 0, 200);
			float r = Random.Range (0, 180);
			destination += Vector3.ProjectOnPlane(transform.right, Vector3.right).normalized * 800 * Mathf.Sin (r);
			destination += Vector3.ProjectOnPlane(transform.forward, Vector3.forward).normalized * 800 * Mathf.Cos(r);
			destination.y = defaultAltitude + Random.Range(0,5);
			myOrder += breakPursuit ;
			myOrder -= shootDown;
			turnRate = 0.005f;
			stopFire();

			//destination = transform.forward*500;
		}
	}

	private void breakPursuit(){
		goTo ();
		//transform.Rotate(Vector3.up*10*Time.deltaTime);
		if (destinationDistance < 200) {
			myOrder += pursuit ;
			myOrder -= breakPursuit;
			turnRate = 0.007f;
		}
	}

	private void approachTarget(){
		destination = target.position;
		headTo ();
		if (destinationDistance < 500) {
			state = "BeginStrafing";
			myOrder -= approachTarget ;
			myOrder += beginStrafing;
			turnRate = 0.005f;
		}
	}

	private void beginStrafing(){
		destination = target.position;
		goTo();
		if (destinationDistance < 300) {
			state = "Strafing";
			myOrder -= beginStrafing ;
			myOrder += strafe ;
			openFire();
		}
	}

	private void strafe(){
		destination = target.position;
		goTo();
		if (destinationDistance < 75) {
			//destination = zoomTarget;
			float r = Random.Range (60, 120);
			destination += Vector3.ProjectOnPlane(transform.right, Vector3.right).normalized * 800 * Mathf.Sin (r);
			destination += Vector3.ProjectOnPlane(transform.forward, Vector3.forward).normalized * 800 * Mathf.Cos(r);
			destination.y = defaultAltitude + Random.Range(0,5);
			stopFire();
			state = "Zoom";
			myOrder -= strafe ;
			myOrder += zoomStrafe;
			turnRate = 0.002f;
		}
	}

	private void zoomStrafe(){
		goTo();
		if (destinationDistance < 200) {
			float r = Random.Range (30, 120);
			destination += Vector3.ProjectOnPlane(transform.right, Vector3.right).normalized* 800 * Mathf.Sin (r);
			destination += Vector3.ProjectOnPlane(transform.forward, Vector3.forward).normalized * 800 * Mathf.Cos(r);
			destination.y = defaultAltitude + Random.Range(0,5);
			state = "Circle";
			myOrder -= zoomStrafe ;
			myOrder += circleTarget;
			turnRate = 0.001f;
		}
	}

	private void circleTarget(){
		//destination = target.position;
		goTo ();
		if (destinationDistance < 200) {
			destination = target.position;
			myOrder -= circleTarget ;
			myOrder += approachTarget;
			state = "Approach";
		}
	}

	void openFire(){
		foreach (ParticleSystem ps in aircraftScript.getgunScripts()){
			ps.Play ();
		}
	}

	void stopFire(){
		foreach (ParticleSystem ps in aircraftScript.getgunScripts()){
			ps.Stop ();
		}
	}

	float projectedDistance(Transform target){
		return projectedDistance(target.position);
	}

	float projectedDistance(Vector3 targetVector){
		return Vector3.Distance(targetVector, new Vector3(transform.position.x, targetVector.y, transform.position.z));
	}
	
}


