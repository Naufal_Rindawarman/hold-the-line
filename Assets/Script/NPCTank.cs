﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NPCTank : NPC {

	public Tank tankScript;

	private WarMachine targetTankScript;
	private int shootBeforeMove = -1;

	private float lostTargetTime = 0f;

	private float startAngle;



	// Use this for initialization

	void Start () {
		if (waypoint != null) {
			dispatchWaypoint (waypoint);
		}
		if (target != null) {
			//issueTarget (target);
		}

		StartCoroutine (checkTarget ());
	}
	
	// Update is called once per frame
	void Update () {
		if (tankScript.isAlive ()) {
			if (myOrder != null) {
				myOrder ();
			}
		}
	}

	IEnumerator checkTarget(){
		while (tankScript.alive) {
			if (LevelManager.units != null) {
				for (int i = 0; i < LevelManager.units.Count; i++) {
					WarMachine wm = LevelManager.units [i];

					if (wm.tag == "Dead") {
						continue;
					}

					if (target != null && wm.transform.Equals (target)) {
						continue;
					}

					if (wm.transform == (transform)) {
						continue;
					}

					//If enemy
					if (wm.team != tankScript.team && wm.tag == "GroundVehicle") {						
						RaycastHit hit;

						if (Physics.Raycast(tankScript.turret.position, wm.transform.position-tankScript.transform.position, out hit)){
							
							if (hit.transform.Equals(wm.transform) && hit.distance < Random.Range(100,450)) {
								if (target == null) {
									issueTarget (wm.transform, Random.Range(2f,5f));
								} else if (hit.distance/Vector3.Distance(transform.position,target.position) < 0.7f && !target.Equals(wm.transform)){
									//Change target if another target closer
									issueTarget (wm.transform, 1f);
								}
							}
						}
					}
				}

			}
			if (target == null) {
				yield return new WaitForSeconds (0.4f);
			} else {
				yield return new WaitForSeconds (1f);
			}

		}
	}

	public void NPCGetHit(WarMachine wmOrigin){
		if (target == null && wmOrigin.tag == "GroundVehicle") {
			if (wmOrigin.team != tankScript.team) {
				issueTarget (wmOrigin.transform, 5f);
			}
		} else if (targetTankScript.isPlayer && !wmOrigin.isPlayer && !wmOrigin.transform.Equals(transform) && wmOrigin.team != tankScript.team && wmOrigin.tag == "GroundVehicle") {
			issueTarget (wmOrigin.transform, 5f);
		}
	}

	public void NPCReload(float t){
		if (shootBeforeMove < 0) {
			shootBeforeMove = Random.Range (2, 7);
		} else if (waypoint != null){
			StopCoroutine ("ignoreTargetFor");
			StartCoroutine (ignoreTargetFor (t * Random.Range(1.5f,2.5f)));
		}
	}

	IEnumerator ignoreTargetFor(float t){
		pauseTarget ();
		resumeWaypoint ();
		yield return new WaitForSeconds (t);
		resumeTarget ();
		stop ();
		shootBeforeMove--;
		yield return new WaitForSeconds (0.1f);
	}
	
				
	private void goToPosition(){	
		if (waypoint != null) {
			tankScript.rotateTo (destination);
			if (Vector3.Angle (transform.forward, destination - transform.position) < 20 || true) {
				tankScript.forward ();
				myOrder -= tankScript.gasIdle;
				//Debug.Log (Vector3.Distance (transform.position, waypoint.position));
				if (Vector3.Distance (transform.position, destination) < 15) {
					Transform nextWaypoint = waypointScript.getRandomNextWaypoint ();
					if (nextWaypoint != null) {
						dispatchWaypoint (nextWaypoint);
					} else {					
						myOrder -= goToPosition;
						myOrder += tankScript.gasIdle;
					}
				}
			} else {
				myOrder += tankScript.gasIdle;
			}
		}
	}

	private void attack(){		

		//Check if enemy destroyed
		if (target) {
			if (target.tag == "Dead") {
				noTarget ();
				resumeWaypoint ();
				return;
			}
			//Quaternion targetRotation = Quaternion.LookRotation (Vector3.Lerp(tankScript.turret.forward, target.position-tankScript.turret.position, 0.05f));
			float angle = Vector3.Angle(tankScript.mainGun.forward, target.position-tankScript.transform.position);		//Debug.Log (anglestartAngle);
			Quaternion localtargetRotation = Quaternion.LookRotation (Vector3.Lerp(tankScript.mainGun.forward, target.position-tankScript.mainGun.position, Mathf.Clamp01(((startAngle-angle)/startAngle)*Time.deltaTime*0.05f)), transform.up);
			//transform.rotation.SetLookRotation(new Vector3(targetRotation.eulerAngles.x,  targetRotation.eulerAngles.y, targetRotation.eulerAngles.z));
			//transform.rotation = targetRotation;
			tankScript.turret.rotation = Quaternion.Euler(transform.eulerAngles.x, localtargetRotation.eulerAngles.y, transform.eulerAngles.z);
			tankScript.mainGun.rotation = Quaternion.Euler(localtargetRotation.eulerAngles.x, tankScript.turret.eulerAngles.y, tankScript.turret.eulerAngles.z);

			RaycastHit hit;

			if (Physics.Raycast (tankScript.turret.position,target.position - tankScript.transform.position, out hit)) {
				if (hit.transform.Equals (target)) {
					lostTargetTime = 0f;
					if (Vector3.Angle (tankScript.mainGun.forward, target.position - tankScript.mainGun.position) < 5) {
						tankScript.Fire ();
					}
				} else {
					lostTargetTime += Time.deltaTime;
					if (lostTargetTime > 5f) {
						lostTargetTime = 0f;
						noTarget ();

					}
				}
			}		
		}

	}

	public void dispatchWaypoint(Transform target){
		myOrder -= goToPosition ;
		waypoint = target;
		destination = waypoint.position + randomXZ();
		waypointScript = target.GetComponent<Waypoint> ();
		myOrder += goToPosition ;
	}

	public void stop(){		
		myOrder -= goToPosition ;
		myOrder += tankScript.gasIdle;
	}

	public void resumeWaypoint(){		
		myOrder += goToPosition ;
	}

	public void issueTarget(Transform newTarget){
		Debug.Log ("Issue target");
		target = newTarget;
		startAngle = Vector3.Angle(tankScript.mainGun.forward, target.position-tankScript.mainGun.position);
		targetTankScript = target.GetComponent<WarMachine> ();
		myOrder += attack ;
		myOrder -= alignTurret;
		myOrder += alignHull;
		stop();
	}

	public void issueTarget(Transform newTarget, float delay){
		issueTarget (newTarget);
		StopCoroutine ("ignoreTargetFor");
		StartCoroutine (ignoreTargetFor (delay));
	}

	public void resumeTarget(){		
		myOrder += attack ;
		myOrder -= alignTurret;
		myOrder += alignHull;
		Debug.Log ("Resume Target");
	}

	public void pauseTarget(){
		myOrder -= attack ;
		myOrder -= alignHull;
		myOrder += alignTurret;
		Debug.Log ("Pause Target");
	}

	public void noTarget(){
		target = null;
		targetTankScript = null;
		myOrder -= attack ;
		myOrder -= alignHull;
		myOrder += alignTurret;
	}

	private void alignTurret(){
		Quaternion targetRotation = Quaternion.LookRotation (Vector3.Lerp(tankScript.turret.forward, transform.forward, 0.1f));
		Quaternion localtargetRotation = Quaternion.LookRotation (Vector3.Lerp(tankScript.mainGun.forward, transform.forward, 0.1f));
		//transform.rotation.SetLookRotation(new Vector3(targetRotation.eulerAngles.x,  targetRotation.eulerAngles.y, targetRotation.eulerAngles.z));
		//transform.rotation = targetRotation;
		tankScript.turret.rotation = Quaternion.Euler(tankScript.turret.eulerAngles.x, localtargetRotation.eulerAngles.y, tankScript.turret.eulerAngles.z);
		tankScript.mainGun.rotation = Quaternion.Euler(localtargetRotation.eulerAngles.x, tankScript.mainGun.rotation.eulerAngles.y, tankScript.mainGun.rotation.eulerAngles.z);

		if (Quaternion.Angle(tankScript.turret.rotation,transform.rotation)<5 && Quaternion.Angle(tankScript.mainGun.rotation,transform.rotation)<5) {
			myOrder -= alignTurret;
		}
	}

	public void alignHull(){
		if (target != null) {
			tankScript.rotateTo (target.position);
		}
	}


}
