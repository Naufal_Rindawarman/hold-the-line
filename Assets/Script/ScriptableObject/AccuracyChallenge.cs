﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class AccuracyChallenge : Challenge{
	public float minAccuracyPercentage = 0.5f;

	public override bool IsCompleted(){
		float percent = LevelManager.levelManagerInstance.GetAccuracy ();
		if (minAccuracyPercentage > percent) {
			return false;
		} else {
			return true;
		}
	}

	public override string GetDescription (){
		return "Accuracy is " + minAccuracyPercentage * 100 + "% or greater";
	}
}
