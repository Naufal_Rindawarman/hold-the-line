﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class HealthChallenge : Challenge{
	public float minHealthPercentage = 0.5f;

	public override bool IsCompleted(){
		float percent = ((float)Player.instance.GetDefenseGunScript ().getCurrentHitPoint () / Player.instance.GetDefenseGunScript ().data.maxHealth);
		Debug.Log (percent);
		if (minHealthPercentage > percent) {
			return false;
		} else {
			return true;
		}
	}

	public override string GetDescription (){
		return "Don't drop below " + minHealthPercentage * 100 + "% of health";
	}
}