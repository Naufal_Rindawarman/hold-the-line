﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class DefenseGunData : WarmachineData {

	public int price = 10000;
	public bool isOwned = false;
}
