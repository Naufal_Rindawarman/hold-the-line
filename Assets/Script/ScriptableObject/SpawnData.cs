﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class SpawnData : ScriptableObject {
	public string name = "Spawn Data";

	public SpawnItem[] spawnList;
}

[Serializable]
public class SpawnItem{
	public WarMachine warMachine;
	public float time = 5;
	public int count = 1;
	public float intervalBetween = 5;
}


