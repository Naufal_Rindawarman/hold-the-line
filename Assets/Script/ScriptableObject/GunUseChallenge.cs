﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class GunUseChallenge : Challenge{
	public DefenseGun useGun;

	public override bool IsCompleted(){
		if (Player.instance.transform.name == useGun.name) {
			return true;
		}
		return false;
	}

	public override string GetDescription (){
		return "Use  " + useGun.name;
	}
}