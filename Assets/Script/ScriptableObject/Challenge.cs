﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public abstract class Challenge : ScriptableObject {
	public string name = "Challenge" ;
	public int rewardCoin = 100;
	public bool isActive = true;
	public bool isCompleted = false;

	public abstract bool IsCompleted ();

	public abstract string GetDescription ();


	public  int TryGetReward(){
		if (IsCompleted()) {
			return rewardCoin;
		} else {
			return 0;
		}
	}

	public string GetReport(){
		if (IsCompleted ()) {
			return (GetDescription() + " [completed] [Coin +" + rewardCoin + "]");
		} else {
			return (GetDescription() + " [failed]");
		}
	}
}







