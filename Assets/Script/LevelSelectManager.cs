﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelSelectManager : MonoBehaviour {

	private static LevelSelectManager _instance;
	public static LevelSelectManager instance {
		get {
			if (_instance == null) {
				_instance = new LevelSelectManager ();
			}
			return _instance;
		}
	}

	//public int selectedLevelIndex = 0; 

	void Awake(){
		if (_instance == null) {
			_instance = this;
		} else if (_instance != this) {
			DestroyImmediate (this);
		}
	}

	public void LoadLevel(){
		SceneManager.LoadScene (GameManager.Instance.selectedLevel);
	}

	public void LoadLevel(int i){
		SceneManager.LoadSceneAsync (i);
	}

	public void SetSelectedLevelIndex(int i){
		GameManager.Instance.selectedLevel = i;
	}
}
