﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class NPCIndicator : MonoBehaviour {

	public Transform canvasPrefab;
	public Vehicle vehicleScript;
	public static Transform canvasObject;
	public Text guiTextIndicator;
	public Image guiHealth;

	private float maxHealth;
	//public Vehicle vehicleScript;
	// Use this for initialization
	void Start () {
		
		guiTextIndicator = Instantiate (guiTextIndicator.gameObject).GetComponent<Text>();
		guiHealth = Instantiate (guiHealth.gameObject).GetComponent<Image>();

		//guiTextText.text = transform.name;
		guiTextIndicator.transform.rotation = CanvasScript.instance.transform.rotation;
		guiTextIndicator.transform.SetParent (IngameGUIScript.instance.npcIndicatorContainer.transform);

		//Health bar
		guiHealth.transform.rotation = CanvasScript.instance.transform.rotation;
		guiHealth.transform.SetParent (IngameGUIScript.instance.npcIndicatorContainer.transform);

		//Max health
		maxHealth = (float)vehicleScript.maxHitPoint;

		//Color
		if (vehicleScript.team == Team.Ally) {
			guiTextIndicator.color = Color.blue;
			guiHealth.color = Color.blue;
		}
	}
	
	// Update is called once per frame
	void Update () {
		//Draw if in screen
		if (Vector3.Angle (Camera.main.transform.forward, transform.position - Camera.main.transform.position) < 60) {
			trackTarget ();
			guiTextIndicator.text = vehicleScript.modelName.ToString ();
			float percent = vehicleScript.getCurrentHitPoint() / maxHealth;
			guiHealth.rectTransform.localScale = new Vector3 (percent, guiHealth.transform.localScale.y, guiHealth.transform.localScale.z);
			guiTextIndicator.enabled = true;
			guiHealth.enabled = true;
		} else {
			guiTextIndicator.enabled = false;
			guiHealth.enabled = false;
		}

		if (!vehicleScript.isAlive ()) {
			guiTextIndicator.gameObject.SetActive (false);
			this.enabled = false;
		}
	}

	void trackTarget(){
		//Camera.main
		Vector3 screenPosition = Camera.main.WorldToScreenPoint (transform.position);
		guiTextIndicator.transform.position = screenPosition + Vector3.up * 45;
		guiHealth.transform.position = screenPosition + Vector3.up * 25;
	}
}
