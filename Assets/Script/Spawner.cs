﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour {

	public SpawnData data;

	public Transform[] vehicles;
	public Team team;

	public Transform[] spawnPoints;

	public float firstDelay = 5f;
	public float spawnPeriod = 10f;

	private float nextSpawn = 0;
	private int waypointLength= 0;
	private int spawnPointsSize= 0;
	private float lastSpawnTime = 0f;

	public Vector2 spawnSpread;

	public static List<SpawnObject> spawnObjects;
	public static bool isInitialized = false;

	public static Spawner allySpawnerInstance;
	public static Spawner enemySpawnerInstance;

	void Start (){		

		if (team == Team.Ally) {
			if (allySpawnerInstance == null) {
				allySpawnerInstance = this;



			} else {
				Destroy (this);
			}
		} else {
			if (enemySpawnerInstance == null) {
				enemySpawnerInstance = this;
				if (data) {
					if (spawnObjects != null) {
						spawnObjects.Clear ();
					}
					spawnObjects = new List<SpawnObject> ();
					enemySpawnerInstance.nextSpawn = firstDelay;
					enemySpawnerInstance.spawnPointsSize = spawnPoints.Length;
					enemySpawnerInstance.initSpawnEnemy ();
				} else {
					StartCoroutine (RandomContinuousSpawn ());
				}

			} else {
				Destroy (this);
			}
		}


	}

	IEnumerator RandomContinuousSpawn(){
		while (Player.instance.GetDefenseGunScript().alive) {
			Spawn(vehicles[Random.Range(0, vehicles.Length)].GetComponent<WarMachine>());
			yield return new WaitForSeconds (Random.Range (5, 7));
		}
	}

	public void initSpawnEnemy(){		
		for (int i = 0; i < data.spawnList.Length; i++) {
			SpawnItem item = data.spawnList [i];
			int count = 0;
			while (count < item.count) {
				if (count == 0) {
					AddSpawnUnit (item.warMachine, item.time); 
				} else {
					AddSpawnUnit (item.warMachine, item.intervalBetween);
				}
				count++;
			}
		}
		StartCoroutine (StartSpawning ());
	}

	// Update is called once per frame
	void Update(){
		if (Input.GetKeyDown(KeyCode.W)){
			if (team == Team.Ally) {
				SpawnAlly ();
			}
		}
	}

	void AddSpawnUnit(WarMachine wm, float timeSinceLastSpawn){		
		if (team == Team.Enemy) {
			LevelManager.levelManagerInstance.enemyReserve++;
		}
		lastSpawnTime += timeSinceLastSpawn;
		spawnObjects.Add (new SpawnObject (wm.gameObject, lastSpawnTime));
	}

	void AddSpawnUnit(int unitIdx, float timeSinceLastSpawn){
		if (unitIdx < vehicles.Length) {
			if (team == Team.Enemy) {
				LevelManager.levelManagerInstance.enemyReserve++;
			}
			lastSpawnTime += timeSinceLastSpawn;
			spawnObjects.Add (new SpawnObject (vehicles [unitIdx].gameObject, lastSpawnTime));
		}
	}

	void AddSpawnUnit(int unitIdx, int timeSinceLastSpawn){
		AddSpawnUnit (unitIdx, (float)timeSinceLastSpawn);
	}

	public void Spawn(WarMachine wm){
		Debug.Log (spawnPointsSize);
		Transform spawnPoint = spawnPoints [(Random.Range (0, spawnPoints.Length))];
		if (wm.tag == "Aircraft") {
			Aircraft v = Instantiate (wm, spawnPoint.position, spawnPoint.rotation).GetComponent<Aircraft> ();
			NPCAircraft airscript = v.GetComponent<NPCAircraft> ();
			airscript.waypoint = spawnPoint;
			v.team = team;
			v.homeBase = transform;
		} else if (wm.tag == "GroundVehicle") {
			Tank v = Instantiate (wm, spawnPoint.position  + new Vector3(Random.Range(-spawnSpread.x,spawnSpread.x), 5, Random.Range(-spawnSpread.y,spawnSpread.y)), spawnPoint.rotation).GetComponent<Tank> ();
			v.team = team;
			NPCTank tankscript = v.GetComponent<NPCTank> ();
			tankscript.waypoint = spawnPoint;
		}
	}

	void SpawnAlly(){
		Transform spawnPoint = spawnPoints [(Random.Range (0, spawnPoints.Length))];
		GameObject wm = vehicles [1].gameObject;
		if (wm.tag == "Aircraft") {
			Aircraft v = Instantiate (wm, spawnPoint.position, spawnPoint.rotation).GetComponent<Aircraft> ();
			NPCAircraft airscript = v.GetComponent<NPCAircraft> ();
			airscript.waypoint = spawnPoint;
			v.team = team;
			v.homeBase = transform;
		} else if (wm.tag == "GroundVehicle") {
			Tank v = Instantiate (wm, spawnPoint.position  + new Vector3(Random.Range(-spawnSpread.x,spawnSpread.x), 5, Random.Range(-spawnSpread.y,spawnSpread.y)), spawnPoint.rotation).GetComponent<Tank> ();
			v.team = team;
			NPCTank tankscript = v.GetComponent<NPCTank> ();
			tankscript.waypoint = spawnPoint;
		}
	}

	IEnumerator StartSpawning(){
		int idx = 0;
		float startTime = Time.time;
		while (idx < spawnObjects.Count) {			
			if (spawnObjects [idx].timeSpawn <= Time.time - startTime) {
				Transform spawnPoint = spawnPoints [(Random.Range (0, spawnPoints.Length))];
				GameObject wm = spawnObjects [idx].warmachine;
				if (wm.tag == "Aircraft") {
					Aircraft v = Instantiate (wm, spawnPoint.position, spawnPoint.rotation).GetComponent<Aircraft> ();
					NPCAircraft airscript = v.GetComponent<NPCAircraft> ();
					airscript.waypoint = spawnPoint;
					v.team = team;
					v.homeBase = transform;
				} else if (wm.tag == "GroundVehicle") {
					Tank v = Instantiate (wm, spawnPoint.position  + new Vector3(Random.Range(-spawnSpread.x,spawnSpread.x), 0, Random.Range(-spawnSpread.y,spawnSpread.y)), spawnPoint.rotation).GetComponent<Tank> ();
					v.team = team;
					NPCTank tankscript = v.GetComponent<NPCTank> ();
					tankscript.waypoint = spawnPoint;
				}
				LevelManager.levelManagerInstance.enemyReserve--;
				idx++;
			}
			yield return null;
		}

	}

	public class SpawnObject{
		public GameObject warmachine;
		public float timeSpawn;

		public SpawnObject(GameObject wm, float timeOfSpawn){
			warmachine = wm;
			timeSpawn = timeOfSpawn;
		}
	}

}


