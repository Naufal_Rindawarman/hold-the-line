﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Player : MonoBehaviour {

	private DefenseGun defenseGunScript;
	public static Player instance;

	public Camera camera;

	public Transform normalPosition;
	public Transform zoomPosition;

	public float normalFOV = 75f;
	public float zoomFOV = 30f;

	public float sensitivityHor = 9.0f;
	public float sensitivityVert = 9.0f;

	public float minimumVert = -10.0f;
	public float maximumVert = 60.0f;

	public float minimumHor = -60.0f;
	public float maximumHor = 60.0f;

	private float _rotationX = 0; 
	private float _rotationY = 0;

	private Vector3 parentRotation;

	private Vector2 firstTouchPos;
	private Vector2 secondTouchPos;
	private int firstTouchId = -1;

	private bool isZoomed = false;

	//GUI
	public GameObject canvasPrefab;
	public static GameObject canvasInstance;

	//Health Bar
	public Image healthBarPrefab;
	public static Image healthBarInstance;
	private float maxHealth;

	//Gun Sight
	public Image sightImagePrefab;
	private Image sightImageInstance;

	private AudioSource turretRotationAudioSource;

	// Use this for initialization
	void Awake(){
		camera = Camera.main;

		if (instance == null) {
			instance = this;
		} else if (instance != this){
			this.enabled = false;
		}

		if (defenseGunScript == null) {
			defenseGunScript = GetComponent<DefenseGun> ();
		} 

		defenseGunScript.isPlayer = true;

		if (camera == null) {
			camera = new GameObject ("MainCamera").AddComponent<Camera> ();
			camera.tag = "MainCamera";
		}			

		if (camera.GetComponent<AudioListener> () == null) {
			camera.gameObject.AddComponent<AudioListener> ();
		}

		LevelManager.CreateLevelManager ();
		Time.timeScale = 1f;
	}

	void Start () {

		turretRotationAudioSource = GetComponent<AudioSource>();

		if (normalPosition == null) {
			normalPosition = zoomPosition;
		}
		if (zoomPosition == null) {
			zoomPosition = defenseGunScript.mainGunMuzzle;
			normalPosition = zoomPosition;
		}
	
		camera.transform.position = normalPosition.position;
		camera.transform.rotation = defenseGunScript.mainGunMuzzle.rotation;
		camera.transform.SetParent (defenseGunScript.elevateBase);

		parentRotation = transform.root.rotation.eulerAngles;
		_rotationX = parentRotation.x;
		_rotationY = parentRotation.y;
		minimumHor = _rotationY + minimumHor;
		maximumHor = _rotationY + maximumHor;
		minimumVert = _rotationX + minimumVert;
		maximumVert = _rotationX + maximumVert;

	}

	void Update () {
		if (defenseGunScript.alive && LevelManager.levelManagerInstance.isGamePlaying) {
			//Firing

			if (Application.platform == RuntimePlatform.WindowsPlayer || Application.platform == RuntimePlatform.WindowsEditor) {
				if (Input.GetButtonDown ("Fire1")) {
					Fire ();
				}

				if (Input.GetButton ("Fire1")) {
					Fire ();
				}

				//Zoom
				if (Input.GetButtonDown ("Fire2")) {
					toggleZoom ();
				}

				if (Input.GetKeyDown (KeyCode.R)) {
					SceneManager.LoadSceneAsync (SceneManager.GetActiveScene ().buildIndex);
				}

				if (Input.GetKeyDown (KeyCode.Delete)) {
					defenseGunScript.getHit (10000, defenseGunScript);
				}

				if (Input.GetKeyDown (KeyCode.T)) {
					for (int i = 0; i < LevelManager.units.Count; i++) {
						WarMachine wm = LevelManager.units [i];
						if (wm.team == Team.Enemy) {
							wm.getHit (10000, defenseGunScript);
						}
					}
				}

				if (Input.GetKeyDown (KeyCode.L)) {
					for (int i = 0; i < WarMachine.units.Count; i++) {
						Debug.Log (WarMachine.units [i].name);
					}
				}

				//Rotation


				float x = Input.GetAxis ("Mouse X");
				float y = Input.GetAxis ("Mouse Y");

				if (x != 0 || y != 0) {
					if (!turretRotationAudioSource.isPlaying) {
						turretRotationAudioSource.Play ();
					}
				} else {
					turretRotationAudioSource.Pause ();
				}

				if (defenseGunScript.rotateBase != null) {
					_rotationY += x * sensitivityHor * Time.deltaTime;
					_rotationY = Mathf.Clamp (_rotationY, minimumHor, maximumHor);
					defenseGunScript.rotateBase.eulerAngles = new Vector3 (defenseGunScript.elevateBase.eulerAngles.x, _rotationY, defenseGunScript.elevateBase.eulerAngles.z);

				}

				if (defenseGunScript.elevateBase != null) {
					_rotationX -= y * sensitivityVert * Time.deltaTime;
					_rotationX = Mathf.Clamp (_rotationX, minimumVert, maximumVert);

					defenseGunScript.elevateBase.eulerAngles = new Vector3 (_rotationX, defenseGunScript.elevateBase.eulerAngles.y, defenseGunScript.elevateBase.eulerAngles.z);
				}


			
			} else if (Application.platform == RuntimePlatform.Android) {
				Touch t = Input.GetTouch (0);
				if (t.phase == TouchPhase.Began && t.position.x < Screen.currentResolution.width / 2 && firstTouchId < 0) {
					firstTouchId = t.fingerId;
					firstTouchPos = t.position;
				}
				if ((t.phase == TouchPhase.Moved || t.phase == TouchPhase.Stationary) && firstTouchId == t.fingerId) {
					Vector2 direction = t.position - firstTouchPos;
					int maxDelta = 400;
					float maxRotate = 80f;

					float x = Mathf.Clamp (direction.x, -maxDelta, maxDelta) / maxDelta;
					float xsmooth = Mathf.Abs (x);
					xsmooth = xsmooth * xsmooth * (3f - (2f * xsmooth));

					float y = Mathf.Clamp (direction.y, -maxDelta, maxDelta) / maxDelta;
					float ysmooth = Mathf.Abs (y);
					ysmooth = ysmooth * ysmooth * (3f - (2f * ysmooth));

					_rotationY += xsmooth * x * maxRotate * Time.deltaTime;
					_rotationY = Mathf.Clamp (_rotationY, minimumHor, maximumHor);
					_rotationX -= ysmooth * y * maxRotate * Time.deltaTime;
					_rotationX = Mathf.Clamp (_rotationX, minimumVert, maximumVert);

					defenseGunScript.rotateBase.eulerAngles = new Vector3 (defenseGunScript.rotateBase.eulerAngles.x, _rotationY, defenseGunScript.rotateBase.eulerAngles.z);
					defenseGunScript.elevateBase.eulerAngles = new Vector3 (_rotationX, defenseGunScript.elevateBase.eulerAngles.y, defenseGunScript.elevateBase.eulerAngles.z);
				}
				if (t.phase == TouchPhase.Ended && firstTouchId == t.fingerId) {
					firstTouchId = -1;
				}
			}
				

		} else if (!defenseGunScript.alive) {
			//Zoom out if die
			isZoomed = true;
			toggleZoom ();
			camera.transform.Translate (0, 5, 5);
			camera.transform.LookAt (transform);
			this.enabled = false;

		} else if (!LevelManager.levelManagerInstance.isGamePlaying) {
			isZoomed = true;
			toggleZoom ();
			camera.transform.Translate (0, 1, -3);
			camera.transform.LookAt (transform);
			this.enabled = false;
		}
	}

	public void Fire (){
		if (defenseGunScript.Fire ()) {
			StopCoroutine (ShakeVertical (2f, 1f, 8));
			StartCoroutine (ShakeVertical (2f,1f, 8));
		}

	}

	public void toggleZoom (){
		if (isZoomed) {
			isZoomed = false;
			camera.transform.position = normalPosition.position;
			camera.fieldOfView = normalFOV;
		} else {
			isZoomed = true;
			camera.transform.position = zoomPosition.position;
			camera.fieldOfView = zoomFOV;
		}
		EffectUIScript.instance.SetActiveSight (isZoomed);
	}

	IEnumerator ShakeVertical(float duration, float amplitude, int rate){
		float t0 = duration;
		while (duration > 0) {			
			//_rotationX = transform.rotation.eulerAngles.x;
			_rotationX -= duration*amplitude*Mathf.Sin ((t0-duration)*rate)*Time.deltaTime;
			defenseGunScript.elevateBase.rotation = Quaternion.Euler (_rotationX, defenseGunScript.elevateBase.eulerAngles.y, defenseGunScript.elevateBase.eulerAngles.z);
			duration -= Time.deltaTime;
			yield return null;
		}
	}

	public DefenseGun GetDefenseGunScript(){
		return defenseGunScript;
	}



}
