﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticleManager : MonoBehaviour {
	public ParticleSystem tankExplosion;
	public ParticleSystem tankSmoke;
	public ParticleSystem tankMuzzleFlash;
	public ParticleSystem tankProjectile;
	public ParticleSystem aircraftProjectile;
	public ParticleSystem tankPlayerProjectile;

	public ParticleSystem hitGround;
	public ParticleSystem hitMetal;

	public static ParticleManager particleManagerInstance;

	void Awake(){
		if (particleManagerInstance == null) {
			particleManagerInstance = this;
		} else {
			Destroy (this);
		}
	}

	public static void Instantiate(){
		if (particleManagerInstance == null) {
			new GameObject ("ParticleManager").AddComponent<ParticleManager> ();
		}
	}
}