﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CallSupportAction : MonoBehaviour {

	public Tank tankPrefab;
	public Aircraft aircraftPrefab;

	public static CallSupportAction instance;

	public int tankCost = 100;
	public int aircraftCost = 200;
	public int artilleryCost = 10;

	public int tankCoolDown = 45;
	public int airCraftCoolDown = 60;

	public int initialSupportPoin = 100;
	public int supportPointRegen = 5;

	public Text supportPointText;
	public Text tankText;
	public Text airCraftText;
	public Text artilleryText;

	private int supportPoint = 0;

	private Image panel;
	private bool isDrawerOpen = true;
	private float openYPosition;

	void Awake(){
		if (instance == null) {
			instance = this;
			panel = GetComponent<Image>();

		} else if (instance != this) {
			Destroy (this);
		}
	}

	// Use this for initialization
	void Start () {
		supportPoint = initialSupportPoin;
		supportPointText.text = "SP " + supportPoint;
		InvokeRepeating ("AddSupportPoint", 0, 1);

		tankText.text = "Tank (" + tankCost + ")";
		airCraftText.text = "Aircraft (" + aircraftCost + ")";
		artilleryText.text = "Artillery (" + artilleryCost + ")";

		openYPosition = panel.rectTransform.anchoredPosition.y;

		ToggleDrawer ();
	}

	void AddSupportPoint(){
		supportPoint += supportPointRegen;
		supportPointText.text = "SP " + supportPoint;
	}

	public void CallTank(){
		if (supportPoint >= tankCost) {
			supportPoint -= tankCost;
			Spawner.allySpawnerInstance.Spawn (tankPrefab);
			VoiceManager.instance.PlayCallAlliedTankClip ();
			supportPointText.text = "SP " + supportPoint;
		}

	}

	public void CallAircraft(){
		if (supportPoint >= aircraftCost) {
			supportPoint -= aircraftCost;
			supportPointText.text = "SP " + supportPoint;
		}
	}

	public void CallArtillery(){
		if (supportPoint >= artilleryCost){
			if (ArtilleryManager.instance.CallArtillery()) {
				supportPoint -= artilleryCost;
				supportPointText.text = "SP " + supportPoint;
			}
		}
	}

	public int GetSupportPoint(){
		return supportPoint;
	}

	public void ToggleDrawer(){
		StopCoroutine ("Animate");
		StartCoroutine ("Animate");
	}


	IEnumerator Animate(){
		float duration = 0f;
		float y;
		float targetY = 0;
		if (!isDrawerOpen) {
			targetY = openYPosition;
		} 
		isDrawerOpen = !isDrawerOpen;
		Vector2 initialPos = panel.rectTransform.anchoredPosition;
		while (duration < 2) {
			y = Mathf.Lerp (panel.rectTransform.anchoredPosition.y, targetY, duration);
			initialPos.y = y;
			panel.rectTransform.anchoredPosition = initialPos;
			duration += Time.deltaTime;
			yield return null;
		}
	}
}
