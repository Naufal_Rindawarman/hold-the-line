﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerTank : MonoBehaviour {

	public Tank tankScript;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		//Player move
		if (Input.GetKey (KeyCode.W)) {
			tankScript.forward ();
		} else {
			tankScript.gasIdle ();
		}

		if (Input.GetKey (KeyCode.S)) {
			tankScript.reverse ();
		}

		//Shooting
		if (Input.GetButtonDown("Fire1")){
			tankScript.Fire ();
		}

		tankScript.transform.Rotate (Vector3.up * Input.GetAxis ("Horizontal") * tankScript.rotateSpeed * Time.deltaTime);

		tankScript.turret.Rotate(Vector3.up*Input.GetAxis ("Mouse X")*tankScript.turretTraverseSpeed*Time.deltaTime);
		tankScript.mainGun.Rotate(Vector3.right*-Input.GetAxis ("Mouse Y")*tankScript.gunElevateSpeed*Time.deltaTime);


		
	}
}
