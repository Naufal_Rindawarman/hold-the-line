﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour {

	public int damage = 10;
	//public AudioClip hitTankClip;
	private ParticleSystem muzzleFlash;
	public ParticleSystem hitGroundPart;
	public ParticleSystem hitTankPart;

	private WarMachine ownerWarmachineScript;
	private GameObject lastHit;
	private WarMachine vehicleScript;
	private ParticleSystem part;
	private List<ParticleCollisionEvent> collisionEvents;

	private AudioSource shootAudioSource;
	private AudioSource hitGroundAudioSource;
	private AudioSource hitMetalAudioSource;

	delegate void CheckHitDelegate(GameObject go);
	CheckHitDelegate myCheckHitDelegate;

	// Use this for initialization
	void Awake(){
		part = GetComponent<ParticleSystem>();
		shootAudioSource = GetComponent<AudioSource> ();
		hitGroundAudioSource = hitGroundPart.GetComponent<AudioSource> ();
		hitMetalAudioSource = hitTankPart.GetComponent<AudioSource> ();
	}

	void Start () {		
		muzzleFlash = ParticleManager.particleManagerInstance.tankMuzzleFlash;
		//hitGroundPart = ParticleManager.particleManagerInstance.hitGround;
		//hitTankPart = ParticleManager.particleManagerInstance.hitMetal;
		//damage = transform.root.GetComponent<WarMachine> ().damage;
		myCheckHitDelegate += firstHitVehicle;
		collisionEvents = new List<ParticleCollisionEvent>();		
	}
	
	void OnParticleCollision(GameObject go){
		int numCollisionEvents = part.GetCollisionEvents(go, collisionEvents);
		var emitParams = new ParticleSystem.EmitParams();
		int i = 0;
		while (i < numCollisionEvents){
			if (go.tag == "GroundVehicle" || go.tag == "Aircraft" || go.tag == "Dead") {
				emitParams.position = collisionEvents [i].intersection;
				emitParams.rotation3D = collisionEvents [i].normal;
				hitTankPart.Emit (emitParams, 2);
				//hitGroundPart.Emit (emitParams, 2);
				PlayHitMetalSound ();
				myCheckHitDelegate (go);
			} else if (go.tag == "Terrain") {		
				emitParams.position = collisionEvents [i].intersection;
				emitParams.rotation3D = collisionEvents [i].normal;
				hitGroundPart.Emit (emitParams, 2);
				playHitGroundSound ();
				//hitTankPart.Emit (emitParams, 100);
			} else {
				LevelManager.levelManagerInstance.MissTarget ();
			}
			i++;
		}
	}

	private void firstHitVehicle (GameObject go){		
		lastHit = go;
		vehicleScript = go.GetComponent<WarMachine>();
		//vehicleScript.setTankHitSound (hitTankClip);
		vehicleScript.getHit (damage, ownerWarmachineScript);
		myCheckHitDelegate -= firstHitVehicle;
		myCheckHitDelegate += hitVehicle;
	}

	private void hitVehicle(GameObject go){
		if (!lastHit.Equals (go)) {
			vehicleScript = go.GetComponent<WarMachine>();
		}
		//vehicleScript.setTankHitSound (hitTankClip);
		vehicleScript.getHit (damage, ownerWarmachineScript);
	}

	public void FireGun(){
		part.Emit (1);
		playGunSound ();
		muzzleFlash.Emit (5);
		if (ownerWarmachineScript == null){
			return;
		}
		if (ownerWarmachineScript.isPlayer) {
			LevelManager.levelManagerInstance.shootTaken++;
		}
	}

	public void FireWithoutFlash(){
		part.Emit (1);
		playGunSound ();
		if (ownerWarmachineScript == null){
			return;
		}
		if (ownerWarmachineScript.isPlayer) {
			LevelManager.levelManagerInstance.shootTaken++;
		}
	}

	public void SetOwner(WarMachine wm){
		ownerWarmachineScript = wm;
	}

	public ParticleSystem GetProjectileParticleSystem(){
		return part;
	}

	void playGunSound(){
		if (!shootAudioSource) {
			return;
		}
		shootAudioSource.Stop ();
		shootAudioSource.Play ();
	}

	void playHitGroundSound(){
		if (!hitGroundAudioSource) {
			return;
		}
		hitGroundAudioSource.Stop ();
		hitGroundAudioSource.Play ();
	}

	void PlayHitMetalSound(){
		if (!hitMetalAudioSource) {
			return;
		}
		hitMetalAudioSource.Stop ();
		hitMetalAudioSource.Play ();
	}

	public void SetShootClip(AudioClip clip){
		shootAudioSource.clip = clip;
	}

	public void SetHitGroundClip(AudioClip clip){
		hitGroundAudioSource.clip = clip;
	}

	public void SetHitMetalClip(AudioClip clip){
		hitMetalAudioSource.clip = clip;
	}
}
