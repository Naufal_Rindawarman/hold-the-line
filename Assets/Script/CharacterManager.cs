﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterManager : MonoBehaviour {

	public static CharacterManager characterManagerinstance;

	public DefenseGun[] defenseGuns;

	public int selectedGunIndex = 0;

	void Awake(){
		if (characterManagerinstance == null) {
			characterManagerinstance = this;
		} else {
			Destroy (gameObject);
		}
		DontDestroyOnLoad (characterManagerinstance.gameObject);
	}

	public void SetSelectedIndex(int i){
		selectedGunIndex = i;
	}
	public int GetSelectedIndex(){
		return selectedGunIndex;
	}


}
