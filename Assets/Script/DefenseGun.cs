﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DefenseGun : WarMachine {

	public Transform rotateBase;
	public Transform elevateBase;

	new void Start(){
		base.Start ();
		blackSmoke.loop = true;
	}

	void Update () {
		if (isAlive()) {
			if (reloadWait > 0) {
				reloadWait -= Time.deltaTime;
			}
		}
	}

	public bool Fire(){
		if (reloadWait <= 0f) {
			projectileScript.FireGun();
			//playGunSound ();
			ammo--;
			reloadWait = fireInterval;

			if (ammo == 0) {
				reloadWait = reloadTime;
				ammo = ammoCapacity;
			}
			return true;
		}
		return false;
	}

}
