﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EndMissionScript : MonoBehaviour {

	public Text titleTextUI;
	public Text scoreTextUI;
	public Text statisticTextUI;
	public Text challengeTextUI;
	public Button nextLevelButton;

	public static EndMissionScript endMissionScriptInstance;

	// Use this for initialization
	void Awake(){
		if (endMissionScriptInstance == null) {
			endMissionScriptInstance = this;
		} else {
			Destroy (this);
		}
		endMissionScriptInstance.gameObject.SetActive (false);
	}

	public void Defeat(){
		LevelManager.levelManagerInstance.isGamePlaying = false;
		gameObject.SetActive (true);
		IngameGUIScript.instance.gameObject.SetActive (false);
		titleTextUI.text = "DEFEAT";
		scoreTextUI.text = "";
		nextLevelButton.gameObject.SetActive (false);
		challengeTextUI.text = "Challenge only available in case of victory";
		SetStatistic ();

	}

	public void Victory(){
		LevelManager.levelManagerInstance.isGamePlaying = false;
		gameObject.SetActive (true);
		IngameGUIScript.instance.gameObject.SetActive (false);
		titleTextUI.text = "VICTORY";
		scoreTextUI.text = "Score " + LevelManager.levelManagerInstance.score;
		nextLevelButton.gameObject.SetActive (true);
		SetStatistic ();
		SetChallengeCompletionReport ();
	}

	void SetStatistic(){
		gameObject.SetActive (true);
		IngameGUIScript.instance.gameObject.SetActive (false);
		int accuracy = 0;
		if (LevelManager.levelManagerInstance.shootTaken > 0) {
			accuracy = LevelManager.levelManagerInstance.shootHit * 100 / LevelManager.levelManagerInstance.shootTaken;
		}
		statisticTextUI.text = accuracy + "%";
		statisticTextUI.text += "\n" + (float)(Player.instance.GetDefenseGunScript().getCurrentHitPoint()*100/Player.instance.GetDefenseGunScript().maxHitPoint) + "%";
		statisticTextUI.text += "\n" + CallSupportAction.instance.GetSupportPoint ();


		//Debug.Log (LevelManager.levelManagerInstance.shootTaken + "," + LevelManager.levelManagerInstance.shootHit);
	}

	void SetChallengeCompletionReport(){
		challengeTextUI.text = "";
		foreach (Challenge ch in LevelManager.levelManagerInstance.challenges) {
			challengeTextUI.text += ch.GetReport () +"\n";
		}
	}
		
}
