﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerSpawner : MonoBehaviour {

	private DefenseGun player;
	public CharacterManager charactermanagerPrefab;

	// Use this for initialization
	void Awake(){		
		if (CharacterManager.characterManagerinstance == null) {
			Instantiate (charactermanagerPrefab);
		}
		player = Instantiate (CharacterManager.characterManagerinstance.defenseGuns [CharacterManager.characterManagerinstance.GetSelectedIndex ()].gameObject,
			transform.position, transform.rotation).GetComponent<DefenseGun> ();
	}
}
