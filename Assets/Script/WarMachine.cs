﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WarMachine : MonoBehaviour {

	public static List<WarMachine> units;

	public WarmachineData data;
	public bool isValueFromData = true;

	public Team team;

	public string modelName ="Vehicle";
	public uint value = 100;

	public int maxHitPoint = 600;
	protected int hitPoint = 600;

	public bool alive = true;
	public bool isPlayer = false;

	//Weapon
	public int damage = 50;
	public float reloadTime = 3f;
	public int fireRate = 120;
	public int ammoCapacity = 5;

	public AudioClip gunSound;
	public AudioClip gunHitSound;

	protected AudioSource gunAudioSource;
	protected AudioSource hitAudioSource;

	public Transform mainGunMuzzle;

	//Particle
	protected static GameObject blackSmokePrefab;
	protected static GameObject explosionPrefab;
	protected static GameObject shootParticlePrefab;
	protected static GameObject playerShootParticlePrefab;

	protected ParticleSystem blackSmoke;
	protected ParticleSystem explosion;
	protected ParticleSystem shootParticle;
	protected Projectile projectileScript;

	protected bool isReloaded = true;
	protected int ammo = 1;
	protected float reloadWait = 0;
	protected float fireInterval = 0.5f;

	public static bool isInitialized = false;

		public virtual void Start (){

		if (isValueFromData && data) {
			maxHitPoint = data.maxHealth;
			name = data.vehicleName;
			damage = data.damage;
			fireRate = data.fireRate;
			ammoCapacity = data.ammoCapacity;
		}

		hitPoint = maxHitPoint;
		ammo = ammoCapacity;
		fireInterval = 60f / (float)fireRate;

		/*
		if (WarMachine.shootParticlePrefab == null) {
			WarMachine.shootParticlePrefab = Resources.Load ("Prefab/Particle/ShootParticle", typeof(GameObject)) as GameObject;
		}
		if (WarMachine.playerShootParticlePrefab == null) {
			WarMachine.playerShootParticlePrefab = Resources.Load ("Prefab/Particle/PlayerShootParticle", typeof(GameObject)) as GameObject;
		}
		if (WarMachine.explosionPrefab == null) {
			//WarMachine.explosionPrefab = Resources.Load ("Prefab/Particle/BigExplosionEffect", typeof(GameObject)) as GameObject;

		}
		if (WarMachine.blackSmokePrefab == null) {
			WarMachine.blackSmokePrefab = Resources.Load ("Prefab/Particle/BlackSmoke", typeof(GameObject)) as GameObject;
		}

		*/
		ParticleManager.Instantiate ();

		if (WarMachine.shootParticlePrefab == null) {
			WarMachine.shootParticlePrefab = ParticleManager.particleManagerInstance.tankProjectile.gameObject;
		}

		if (WarMachine.playerShootParticlePrefab == null) {
			WarMachine.playerShootParticlePrefab = ParticleManager.particleManagerInstance.tankPlayerProjectile.gameObject;
		}
		if (WarMachine.explosionPrefab == null) {
			WarMachine.explosionPrefab = ParticleManager.particleManagerInstance.tankExplosion.gameObject;
		}
		if (WarMachine.blackSmokePrefab == null) {
			WarMachine.blackSmokePrefab = ParticleManager.particleManagerInstance.tankSmoke.gameObject;
		}




		blackSmoke = GameObject.Instantiate (WarMachine.blackSmokePrefab, transform.position, Quaternion.LookRotation (transform.up)).GetComponent<ParticleSystem> ();
		blackSmoke.transform.SetParent (transform);
		blackSmoke.gameObject.SetActive (false);
		explosion = GameObject.Instantiate (WarMachine.explosionPrefab, transform.position, Quaternion.LookRotation (transform.up)).GetComponent<ParticleSystem> ();
		explosion.transform.SetParent (transform);
		explosion.gameObject.SetActive (false);
		if (mainGunMuzzle != null) {
			if (isPlayer) {
				shootParticle = GameObject.Instantiate (WarMachine.playerShootParticlePrefab, mainGunMuzzle.position, mainGunMuzzle.rotation).GetComponent<ParticleSystem> ();
			} else {
				shootParticle = GameObject.Instantiate (WarMachine.shootParticlePrefab, mainGunMuzzle.position, mainGunMuzzle.rotation).GetComponent<ParticleSystem> ();
			}
			shootParticle.transform.SetParent (mainGunMuzzle);

			gunAudioSource = shootParticle.GetComponent<AudioSource> ();
			if (gunSound != null) {
				gunAudioSource.clip = gunSound;
			}

			projectileScript = shootParticle.GetComponent<Projectile> ();

			if (gunHitSound != null) {
				projectileScript.SetHitMetalClip(gunHitSound);

			}


			projectileScript.damage = damage;
			projectileScript.SetOwner (this);
		
			if (isPlayer) {
				setGunConvergence (0f);
			}

			if (team == Team.Enemy) {
				LevelManager.levelManagerInstance.enemyAlive++;
			}
		}

		hitAudioSource = transform.GetComponent<AudioSource> ();

		//shootParticle.gameObject.SetActive (false);

		LevelManager.units.Add (this);
	}

	/*
	public void damageOpponent(WarMachine opponent){
		opponent.getHit (damage, this);
	}
	*/

	public int getCurrentHitPoint(){
		return hitPoint;
	}

	public void getHit(int i, WarMachine origin){
		if (alive) {
			hitPoint -= i;


			if (isPlayer) {
				PlayerHealthBarScript.instance.UpdateHealthBar (((float)hitPoint/(float)maxHitPoint));
				EffectUIScript.instance.ShowGetHitOverlay ();
				LevelManager.levelManagerInstance.hitTaken++;
			} else {
				if (origin != null) {
					gameObject.SendMessage ("NPCGetHit", origin);
					if (origin.isPlayer) {
						LevelManager.levelManagerInstance.shootHit++;
					}
				}
			}

			if (hitPoint <= 0) {
				hitPoint = 0;
				if (origin == null) {
					if (!isPlayer && team == Team.Enemy) {
						LevelManager.levelManagerInstance.AddScoreWithoutMultiplier ((int)value);
					}
				} else if (!isPlayer && team == Team.Enemy && origin.isPlayer) {
					LevelManager.levelManagerInstance.AddScore ((int)value);

				}
				HitReport.instance.PlayerReportDestroyed ();
				destroyThis ();
			} else {

				if (origin == null) {
					if (!isPlayer && team == Team.Enemy) {
						
					}
				} else if (!isPlayer && team == Team.Enemy && origin.isPlayer) {
					HitReport.instance.PlayerReportHit ();
					LevelManager.levelManagerInstance.HitTarget ();
				}
			}

		}
		//playGetHitSound();
	}

	public bool isAlive(){
		return alive;
	}
		
	public void destroyThis ()	{
		transform.tag = "Dead";
		alive = false;
		blackSmoke.gameObject.SetActive (true);;
		explosion.gameObject.SetActive (true);
		if (team == Team.Enemy) {
			LevelManager.levelManagerInstance.enemyAlive--;
			LevelManager.levelManagerInstance.DestroyTarget ();
		}

		if (!isPlayer) {
			Invoke ("RemoveFromMap", 15f);
		} else {
			LevelManager.levelManagerInstance.Lose ();
		}
		//LevelManager.units.Remove(this);
	}

	public void playGetHitSound(){
		hitAudioSource.Stop ();
		hitAudioSource.Play ();
	}

	public void playGunSound(){
		gunAudioSource.Stop ();
		gunAudioSource.Play ();
	}

	public void setTankHitSound(AudioClip hitClip){
		hitAudioSource.clip = hitClip;
	}

	public void setGunConvergence(float f){
		/*
		SerializedObject thisParticle = new SerializedObject (shootParticle);
		thisParticle.FindProperty ("ShapeModule.angle").floatValue = f;
		thisParticle.ApplyModifiedProperties ();
		*/
	}

	void RemoveFromMap(){
		gameObject.SetActive (false);
	}
}
