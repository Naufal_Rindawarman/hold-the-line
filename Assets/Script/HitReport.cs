﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HitReport : MonoBehaviour {

	public Text textPrefab;

	private Text[] textReports;

	public static HitReport instance;
	private int currentIndex = 0;
	private Vector3 initialPosition;

	void Awake(){
		if (instance == null) {
			instance = this;
		} else {
			Destroy (gameObject);
		}
	}

	// Use this for initialization
	void Start () {
		initialPosition = textPrefab.rectTransform.position;
		textReports = new Text[10];
		for (int i = 0; i < textReports.Length; i++) {
			textReports [i] = Instantiate (textPrefab, initialPosition, textPrefab.transform.rotation, transform);
			textReports [i].text = "";
		}
		textPrefab.gameObject.SetActive (false);

	}

	IEnumerator report(string s){
		float duration = 0f;
		Text textElement = textReports [currentIndex];
		textElement.transform.position = initialPosition;
		textElement.text = s;
		currentIndex++;
		if (currentIndex >= textReports.Length) {
			currentIndex = 0;
		}
		//yield return new WaitForSeconds(1f);
		while (duration < 2.5f){
			textElement.transform.Translate (Vector3.up*30*Time.deltaTime);
			duration += Time.deltaTime;
			yield return null;
		}
		textElement.text = "";
	}

	public void PlayerReportHit(){
		StartCoroutine (report ("Enemy Hit"));
	}

	public void PlayerReportDestroyed(){
		StartCoroutine (report ("Enemy Destroyed"));
	}
}
