﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Aircraft : Vehicle {
	
	public float angle = 0f;
	public Transform target;
	public Transform homeBase;

	private float defaultAltitude;

	protected static GameObject aircraftGunPrefab;

	public Transform[] guns;
	private ParticleSystem[] shootParticles;
	//private AudioSource gunAudioSource;

	private float targetX;
	private float targetZ;
	private float targetDistance;
	private float elevation;

	private bool firing = false;
	public Vector3 zoomTarget;
	public Vector3 normalTarget;
	private float breakPursuitTime = 0f;

	private Vector3 beginBoomPos;
	private string prevTargetTag = "";

	public ParticleSystem[] cannons;


	public Vehicle vehicleScript;
	public Transform[] propeller;

	private bool isDowning = false;
	private Rigidbody rb;
	private float cannonfirerate = 0.1f;
	private float cannonfirewait = 0;

	new void Start () {
		base.Start ();
		defaultAltitude = transform.position.y;
		rb = GetComponent<Rigidbody> ();

		if (Aircraft.aircraftGunPrefab == null) {
			//Aircraft.aircraftGunPrefab = Resources.Load ("Prefab/Particle/AircraftGun", typeof(GameObject)) as GameObject;
			Aircraft.aircraftGunPrefab = ParticleManager.particleManagerInstance.aircraftProjectile.gameObject;
		}

		shootParticles = new ParticleSystem[guns.Length];
		for (int i = 0; i<guns.Length; i++) {
			shootParticles[i] = GameObject.Instantiate(Aircraft.aircraftGunPrefab,guns[i].position, guns[i].rotation).GetComponent<ParticleSystem>();
			shootParticles[i].transform.SetParent (guns[i]);
			gunAudioSource = shootParticles [i].transform.GetComponent<AudioSource> ();
		}
	}
	
	// Update is called once per frame
	void Update () {		
		forward ();
		if (shootParticles [0].isPlaying) {
			if (cannonfirewait<0) {
				gunAudioSource.Play ();
				cannonfirewait = cannonfirerate;
			}
		} else {
			//if (gunAudioSource.isPlaying) {
				gunAudioSource.Stop ();
			//}
		}
		cannonfirewait -= Time.deltaTime;

		if (!alive) {
			transform.Translate (Vector3.up * -5 * Time.deltaTime, Space.World);
			transform.Rotate (Vector3.forward * 360 * Time.deltaTime);
			transform.Rotate (Vector3.right * -45 * Time.deltaTime);
			if (transform.position.y < 0) {
				
			}
		}
	}

	public void forward(){
		transform.Translate (Vector3.forward * maxSpeed * Time.deltaTime);
		//rb.AddForce(transform.forward*50*Time.deltaTime);
	}

	public void turnTo(Vector3 target){
		//Quaternion targetRotation = Quaternion.LookRotation (target - transform.position);
	}

	public ParticleSystem[] getgunScripts(){
		return shootParticles;
	}
}
