﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GunSelectManager : MonoBehaviour {

	public static GunSelectManager gunSelectManagerInstance;
	public Transform spawnPosition;

	public Text gunNameText;
	public Text gunStatsValuesText;

	private DefenseGun[] guns;

	private int selectedIndex = 0;

	private Vector3 direction;

	private Coroutine panCoroutine;

	// Use this for initialization
	void Awake () {
		if (gunSelectManagerInstance == null) {
			gunSelectManagerInstance = this;
		} else {
			Destroy (gameObject);
		}
		a ();
	}

	void a(){
		DefenseGun[] gunPrefabs = CharacterManager.characterManagerinstance.defenseGuns;
		guns = new DefenseGun[gunPrefabs.Length];
		Quaternion previousRotation = spawnPosition.rotation;
		Vector3 previousPosition = spawnPosition.position;
		for (int i = 0; i < gunPrefabs.Length; i++) {
			guns [i] = Instantiate (gunPrefabs [i].gameObject, previousPosition, previousRotation, spawnPosition).GetComponent<DefenseGun> ();
			guns [i].enabled = false;
			guns [i].GetComponent<Player> ().enabled = false;
			if (i == 0) {
				continue;
			}
			guns [i].transform.Translate (10, 0, 0);
			guns [i].transform.Rotate (0, 10, 0);
			previousRotation = guns [i].transform.rotation;
			previousPosition = guns [i].transform.position;

			//guns [i].gameObject.SetActive (false);
		}

		UpdateView ();
		direction = Camera.main.transform.position - guns [selectedIndex].transform.position;
		StartPanCamera (Camera.main.transform.forward);
	}

	void Update(){
		if (Input.GetKeyDown(KeyCode.RightArrow)){
			SelectPrevious ();
			Debug.Log ("Previous");
		}

		if (Input.GetKeyDown(KeyCode.LeftArrow)){
			SelectNext ();
			Debug.Log ("Next");
		}

		//spawnPosition.Rotate (0, 30 * Time.deltaTime, 0);
	}

	public void SelectNext(){
		//guns [selectedIndex].gameObject.SetActive (false);
		Vector3 lastTarget = guns [selectedIndex].transform.position;
		selectedIndex++;
		if (selectedIndex >= guns.Length) {
			selectedIndex = 0;
		}
		CharacterManager.characterManagerinstance.selectedGunIndex = selectedIndex;
		UpdateView ();

		StartPanCamera (lastTarget);

	}

	public void SelectPrevious(){
		//guns [selectedIndex].gameObject.SetActive (false);
		Vector3 lastTarget = guns [selectedIndex].transform.position;
		selectedIndex--;
		if (selectedIndex < 0) {
			selectedIndex = guns.Length-1;
		}
		CharacterManager.characterManagerinstance.selectedGunIndex = selectedIndex;
		UpdateView ();

		StartPanCamera(lastTarget);
	}

	void StartPanCamera(Vector3 target){
		if (panCoroutine != null) {
			StopCoroutine (panCoroutine);
		}
		panCoroutine = StartCoroutine(PanCamera(target));
	}

	IEnumerator PanCamera(Vector3 lastTarget){
		Camera cam = Camera.main;
		float duration = 0f;
		//Vector3 direction = cam.transform.position - lastTarget;
		Vector3 targetPos = guns [selectedIndex].transform.position + direction;
		while (duration < 5) {			
			cam.transform.position = Vector3.Lerp (cam.transform.position, targetPos, duration/5);
			//cam.transform.LookAt (guns [selectedIndex].transform.position);
			cam.transform.rotation = Quaternion.Lerp(cam.transform.rotation, Quaternion.LookRotation(guns [selectedIndex].transform.position+Vector3.up*3-cam.transform.position), duration/5);
			duration += Time.deltaTime;
			//duration = Mathf.Clamp01 (duration);
			yield return null;
		}

	}

	void UpdateView(){
		//guns [selectedIndex].gameObject.SetActive (true);
		gunNameText.text = guns [selectedIndex].data.name;
		gunStatsValuesText.text = guns [selectedIndex].data.maxHealth.ToString();
		gunStatsValuesText.text += "\n" + guns [selectedIndex].data.damage.ToString();
		gunStatsValuesText.text += "\n" + guns [selectedIndex].data.reloadTime.ToString();

	}

	public void SelectGun(int i){
		CharacterManager.characterManagerinstance.SetSelectedIndex (i);
	}

}
