﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NPC : MonoBehaviour {

	public Transform waypoint;
	public Transform target;

	protected Vector3 destination; //Target + RandomVector
	protected float destinationDistance;

	protected Waypoint waypointScript;

	protected delegate void Orders();
	protected Orders myOrder;

	protected Vector3 randomXZ(){
		return new Vector3 (Random.Range (-5, 5), 0, Random.Range (-5, 5));
	}
}
