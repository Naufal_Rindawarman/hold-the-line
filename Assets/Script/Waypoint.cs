﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Waypoint : MonoBehaviour {

	public Transform[] nextWaypoint;
	public Transform[] previousWaypoint;

	public Transform getRandomNextWaypoint(){
		if (nextWaypoint.Length > 0) {
			return nextWaypoint [Random.Range (0, nextWaypoint.Length)];
		} else {
			return null;
		}
	}
}
