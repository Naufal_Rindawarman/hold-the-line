﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Aircraft : Vehicle {

	public enum AircraftState
	{
		Approach,
		Boom,
		Zoom,
		Leveling,
		Circle,
		Pursuit,
		BreakPursuit
	}

	public float angle = 0f;
	public Transform target;
	public Transform homeBase;

	private float defaultAltitude;

	private float targetX;
	private float targetZ;
	private float targetDistance;
	private float elevation;

	private bool firing = false;
	public Vector3 zoomTarget;
	public Vector3 normalTarget;
	public AircraftState state = AircraftState.Approach;
	private float breakPursuitTime = 0f;

	private Vector3 beginBoomPos;
	private string prevTargetTag = "";

	public ParticleSystem[] cannons;

	public Vehicle vehicleScript;
	public Transform[] propeller;


	void Start () {
		defaultAltitude = transform.position.y;


		blackSmoke = GameObject.Instantiate(blackSmoke.transform, transform.position, Quaternion.LookRotation(transform.up)).GetComponent<ParticleSystem>();
		blackSmoke.transform.SetParent (transform);
		explosion = GameObject.Instantiate(explosion.transform, transform.position, Quaternion.LookRotation(transform.up)).GetComponent<ParticleSystem>();
		explosion.transform.SetParent (transform);
	}

	// Update is called once per frame
	void Update () {

		if (alive) {

			if (target != null) {
				if (target.tag == "GroundVehicle") {
					if (prevTargetTag != target.tag) {
						prevTargetTag = target.tag;
						state = AircraftState.Approach;
					}
					targetGround (target);
				} else if (target.tag == "Aircraft") {
					if (prevTargetTag != target.tag) {
						prevTargetTag = target.tag;
						state = AircraftState.Pursuit;
					}
					targetAir (target);
				} else if (target.tag == "Dead") {
					target = null;
					stopFire ();

				}



			} else {
				headTo (homeBase.position);
			}

			foreach (Transform prop in propeller) {
				prop.Rotate (Vector3.up * 360 * Time.deltaTime);
			}

		} else {
			stopFire ();
			transform.Translate (Vector3.forward * maxSpeed * Time.deltaTime);
			transform.Translate (Vector3.up * -100 * Time.deltaTime, Space.World);
			transform.Rotate (Vector3.forward * 360 * Time.deltaTime);
			transform.Rotate (Vector3.right * -45 * Time.deltaTime);
			if (transform.position.y < 0) {
				Destroy (gameObject);
			}
		}

	}

	void assignTarget(Transform target){

	}

	void targetAir(Transform target){
		if (state == AircraftState.Pursuit) {
			Quaternion targetRotation = Quaternion.LookRotation (Vector3.Lerp(transform.forward, target.position-transform.position, 0.02f*Time.deltaTime));
			transform.rotation = targetRotation;
			targetDistance = Vector3.Distance(target.position, transform.position);
		} else if (state == AircraftState.BreakPursuit) {
			Quaternion targetRotation = Quaternion.LookRotation (Vector3.Lerp(transform.forward, zoomTarget, 0.009f*Time.deltaTime));
			transform.rotation = targetRotation;
			targetDistance = Vector3.Distance(target.position, transform.position);
		}

		if (targetDistance < 300 && !firing && state == AircraftState.Pursuit) {
			if (Vector3.Angle (transform.forward, target.position - transform.position) < 1) {
				openFire ();
			} else {
				stopFire ();
				//state = AircraftState.BreakPursuit;
			}
		} else if (targetDistance < 20 && firing && state == AircraftState.Pursuit) {
			stopFire ();
			state = AircraftState.BreakPursuit;
			zoomTarget = target.position - transform.position + new Vector3 (50, Random.Range(-1,1)*10, 88);
		} else if (state == AircraftState.BreakPursuit) {
			breakPursuitTime += Time.deltaTime;
			if (breakPursuitTime > 8) {
				state = AircraftState.Pursuit;
				breakPursuitTime = 0;
			}
		}
	}

	void headTo(){
		transform.Rotate (Vector3.up * angle * Time.deltaTime, Space.World);

		//Debug.Log (transform.rotation.eulerAngles.x);
		//Debug.Log (transform.rotation.eulerAngles.y);
		//Debug.Log (transform.rotation.eulerAngles.z);

		//transform.rotation =  Quaternion.Lerp(transform.rotation, Quaternion.Euler(transform.rotation.eulerAngles+(Vector3.forward*90)),  0.05f);
		float roll = Mathf.LerpAngle(transform.rotation.eulerAngles.z, -angle, 0.1f);
		transform.rotation = Quaternion.Euler(transform.rotation.eulerAngles.x, transform.rotation.eulerAngles.y, roll);
	}

	void headTo(Vector3 vector){
		Quaternion targetRotation = Quaternion.LookRotation (Vector3.Lerp(transform.forward, new Vector3(vector.x, transform.position.y, vector.z)-transform.position, 0.009f*Time.deltaTime));
		transform.rotation = targetRotation;
		//float roll = Mathf.LerpAngle(transform.rotation.eulerAngles.z, -angle, 0.1f);
		//transform.rotation = Quaternion.Euler(transform.rotation.eulerAngles.x, transform.rotation.eulerAngles.y, roll);
	}

	void targetGround(Transform target){


		//Quaternion targetRotation = Quaternion.LookRotation (Vector3.Lerp(transform.forward, target.position-transform.position, 0.5f*Time.deltaTime));
		//transform.rotation = targetRotation;
		if (state == AircraftState.Boom) {
			Quaternion targetRotation = Quaternion.LookRotation (Vector3.Lerp(transform.forward, target.position-transform.position, 0.006f*Time.deltaTime));
			transform.rotation = targetRotation;
			targetDistance = Vector3.Distance(target.position, new Vector3(transform.position.x, target.position.y, transform.position.z));
		} else if (state == AircraftState.Approach) {
			Quaternion targetRotation = Quaternion.LookRotation (Vector3.Lerp(transform.forward, new Vector3(target.position.x, transform.position.y, target.position.z)-transform.position, 0.002f*Time.deltaTime));
			elevation = Mathf.LerpAngle (transform.rotation.eulerAngles.x, 0, 0.01f);
			//transform.rotation = targetRotation;
			transform.rotation = Quaternion.Euler (elevation, targetRotation.eulerAngles.y, transform.eulerAngles.z);
			targetDistance = Vector3.Distance(target.position, new Vector3(transform.position.x, target.position.y, transform.position.z));
		} else if (state == AircraftState.Zoom) {
			Quaternion targetRotation = Quaternion.LookRotation (Vector3.Lerp(transform.forward, zoomTarget-transform.position, 0.002f*Time.deltaTime));
			//transform.rotation = Quaternion.Euler (targetRotation.eulerAngles.x, targetRotation.eulerAngles.y, transform.eulerAngles.z);
			transform.rotation = targetRotation;
			targetDistance = Vector3.Distance(zoomTarget, new Vector3(transform.position.x, zoomTarget.y, transform.position.z));
		}
		//transform.rotation = Quaternion.Euler(transform.rotation.eulerAngles.x, targetRotation.eulerAngles.y, transform.eulerAngles.z);
		//transform.Rotate (Vector3.up * a * Time.deltaTime, Space.World);

		if (targetDistance < 400 && state == AircraftState.Approach) {
			//defaultAltitude = transform.position.y;
			zoomTarget = ((transform.forward * 1000 + Vector3.up * defaultAltitude));
			//normalTarget = ((transform.forward * 300 + Vector3.up * defaultAltitude));
			state = AircraftState.Boom;
		} else if (targetDistance < 300 && state == AircraftState.Boom && !firing) {
			if (Vector3.Angle (transform.forward, target.position - transform.position) < 10) {
				openFire ();
			}
		} else if (targetDistance < 50 && state == AircraftState.Boom && firing) {
			state = AircraftState.Zoom;
			stopFire ();
		} else if (targetDistance < 50 && state == AircraftState.Zoom) {
			state = AircraftState.Approach;
		} 


		//Debug.Log (transform.rotation.eulerAngles.x);
		//Debug.Log (transform.rotation.eulerAngles.y);
		//Debug.Log (transform.rotation.eulerAngles.z);

		//transform.rotation =  Quaternion.Lerp(transform.rotation, Quaternion.Euler(transform.rotation.eulerAngles+(Vector3.forward*90)),  0.05f);
		//float roll = Mathf.LerpAngle(transform.rotation.eulerAngles.z, -angle, 0.1f);
		//transform.rotation = Quaternion.Euler(transform.rotation.eulerAngles.x, transform.rotation.eulerAngles.y, roll);

	}

	void openFire(){
		foreach (ParticleSystem ps in cannons){
			ps.Play ();
		}
		firing = true;
	}

	void stopFire(){
		foreach (ParticleSystem ps in cannons){
			ps.Stop ();
		}
		firing = false;
	}

	void OnTriggerEnter(Collider other) {
		Debug.Log ("Trigger");
		if (other.tag == "Aircraft" && target == null) {
			vehicleScript = other.GetComponent<Aircraft> ();
			//If meet enemy 
			if (!vehicleScript.team.Equals (team)) {
				target = (other.transform);
			}
		}
	}

	public void forward(){
		transform.Translate (Vector3.forward * maxSpeed * Time.deltaTime);
	}
}
