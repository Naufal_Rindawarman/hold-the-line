﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraScript : MonoBehaviour {

	public Transform target;
	private Vector3 offsetPosition;

	// Use this for initialization
	void Start () {
		offsetPosition = transform.position - target.position;
	}
	
	// Update is called once per frame
	void Update () {		
		transform.SetPositionAndRotation (target.position - new Vector3(0, -4, 5) , target.rotation);
	}
}
