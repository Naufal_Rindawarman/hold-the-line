﻿using UnityEngine;
using System.Collections;

[AddComponentMenu("Control Script/Mouse Look")]

public class MouseLook : MonoBehaviour {

	public enum RotationAxes {
        MouseXAndY = 0,
        MouseX = 1,
        MouseY = 2

    }
    public RotationAxes axes = RotationAxes.MouseXAndY;

    public float sensitivityHor = 9.0f;
    public float sensitivityVert = 9.0f;

    public float minimumVert = -10.0f;
    public float maximumVert = 60.0f;

	public float minimumHor = -60.0f;
	public float maximumHor = 60.0f;

    private float _rotationX = 0; 
	private float _rotationY = 0;

	private Vector3 parentRotation;

	void Start(){
		parentRotation = transform.root.rotation.eulerAngles;
		_rotationX = parentRotation.x;
		_rotationY = parentRotation.y;
		minimumHor = _rotationY + minimumHor;
		maximumHor = _rotationY + maximumHor;
		minimumVert = _rotationX + minimumVert;
		maximumVert = _rotationX + maximumVert;
	}

    // Update is called once per frame
    void Update () {
        if (axes == RotationAxes.MouseX) {
			_rotationY += Input.GetAxis("Mouse X") * sensitivityHor;
			_rotationY = Mathf.Clamp(_rotationY, minimumHor, maximumHor);

			//float rotationX = transform.localEulerAngles.x;

			transform.eulerAngles = new Vector3(parentRotation.x, _rotationY, 0);

        }
        else if (axes == RotationAxes.MouseY) {
            _rotationX -= Input.GetAxis("Mouse Y") * sensitivityVert;
            _rotationX = Mathf.Clamp(_rotationX, minimumVert, maximumVert);

            //float rotationY = transform.localEulerAngles.y;

			transform.eulerAngles = new Vector3(_rotationX, parentRotation.y, 0);

        }
        else {
			_rotationY += Input.GetAxis("Mouse X") * sensitivityHor;
			_rotationY = Mathf.Clamp(_rotationY, minimumHor, maximumHor);

			_rotationX -= Input.GetAxis("Mouse Y") * sensitivityVert;
			_rotationX = Mathf.Clamp(_rotationX, minimumVert, maximumVert);

			transform.eulerAngles = new Vector3(_rotationX, _rotationY, 0);

        }

	}
}
