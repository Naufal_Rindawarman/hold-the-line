﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class IngameGUIScript : MonoBehaviour {

	public static IngameGUIScript instance;
	public Text scoreTextUI;
	public GameObject npcIndicatorContainer;

	// Use this for initialization
	void Awake() {
		if (instance == null) {
			instance = this;
		} else {
			Destroy (this);
		}

		if (npcIndicatorContainer == null) {
			npcIndicatorContainer = gameObject;
		}
	}
}
