﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tank : Vehicle {


	public enum Category {LightTank, MediumTank, HeavyTank};

	public Category category;

	public float forwardReverseRatio = 2f;

	public float turretTraverseSpeed = 10f;
	public float gunElevateSpeed = 20f;

	public int gunMinElevation = 5;
	public int gunMaxElevation = 45;

	public Transform turret;
	public Transform mainGun;

	private float maxReverseSpeed = 25;

	private float acc = 0.1f;

	// Use this for initialization
	new void Start () {
		base.Start ();
		acc = acceleration / Mathf.Pow (maxSpeed, 2);
		maxReverseSpeed = maxSpeed / forwardReverseRatio * -1;
		ammo = ammoCapacity;
		gunMaxElevation = 360 - gunMaxElevation;

	}
	
	// Update is called once per frame
	void Update () {
		if (isAlive()) {
			if (reloadWait > 0) {
				reloadWait -= Time.deltaTime;
			}

			transform.Translate (Vector3.forward * currentSpeed * Time.deltaTime);


			//Limit traverse elevation
			if (mainGun.rotation.eulerAngles.x > gunMinElevation && mainGun.rotation.eulerAngles.x < 180) {
				mainGun.rotation = Quaternion.Euler (gunMinElevation, mainGun.rotation.eulerAngles.y, mainGun.rotation.eulerAngles.z);
			} else if (mainGun.rotation.eulerAngles.x < gunMaxElevation && mainGun.rotation.eulerAngles.x > 180) {
				mainGun.rotation = Quaternion.Euler (gunMaxElevation, mainGun.rotation.eulerAngles.y, mainGun.rotation.eulerAngles.z);
			}	
		}
	}

	public void forward(){	
		currentSpeed = Mathf.Lerp (currentSpeed, (float)maxSpeed, acc*Time.deltaTime);	
	}

	public void reverse(){
		if (currentSpeed > 1) {
			currentSpeed = Mathf.Lerp (currentSpeed, 0f, 0.2f);
		} 
		else {
			currentSpeed = Mathf.Lerp (currentSpeed, maxReverseSpeed, acc);
		}
	}

	public void rotateTo(Vector3 target){
		Quaternion targetRotation = Quaternion.LookRotation (Vector3.Lerp(transform.forward, target-transform.position, 0.05f));
		targetRotation = Quaternion.Lerp (transform.rotation, targetRotation, 0.5f*Time.deltaTime);
		transform.rotation = Quaternion.Euler(transform.eulerAngles.x, targetRotation.eulerAngles.y, transform.eulerAngles.z);
	}

	public bool Fire(){
		if (reloadWait <= 0f) {
			projectileScript.FireGun ();
			ammo--;
			reloadWait = fireInterval;

			if (ammo == 0) {
				reloadWait = reloadTime;
				ammo = ammoCapacity;
				if (!isPlayer) {
					gameObject.SendMessage ("NPCReload", reloadTime);
				}
			}
			return true;
		}
		return false;
	}

	public float getCurrentSpeed(){
		return currentSpeed;
	}

	public float getReloadWaitingTime(){
		return reloadWait;
	}

	public void gasIdle(){
		currentSpeed = Mathf.Lerp (currentSpeed, 0f, 0.1f);
	}



}
