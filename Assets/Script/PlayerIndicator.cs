﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class PlayerIndicator : MonoBehaviour {

	public DefenseGun playerGunScript;
	public Image healthBar;

	private float maxHealth;

	private float hColor;
	private float sColor;
	private float vColor;

	// Use this for initialization
	void Start () {
		if (playerGunScript == null) {
			playerGunScript = GetComponent<DefenseGun> ();
		}
		maxHealth = (float) playerGunScript.getCurrentHitPoint();
		Color.RGBToHSV(healthBar.color, out hColor, out sColor,  out vColor);
	}


}
