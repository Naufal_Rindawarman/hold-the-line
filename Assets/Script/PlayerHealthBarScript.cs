﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerHealthBarScript : MonoBehaviour {

	public Slider healthSlider;
	public Image healthFill;

	private float hColor;
	private float sColor;
	private float vColor;

	public static PlayerHealthBarScript instance;

	void Awake () {
		if (instance == null) {
			instance = this;
		} else {
			Destroy (gameObject);
		}
		Color.RGBToHSV(healthFill.color, out hColor, out sColor,  out vColor);
	}

	public void UpdateHealthBar(float percent){
		healthSlider.value = percent;
		//Debug.Log (percent);
		healthFill.color = Color.HSVToRGB (percent*hColor, sColor, vColor);
	}
}
