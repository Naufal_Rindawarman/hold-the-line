﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MobileControlGUI : MonoBehaviour {

	// Use this for initialization
	void Start () {
		if (Application.platform == RuntimePlatform.Android || Application.platform == RuntimePlatform.IPhonePlayer) {
			gameObject.SetActive(true);
		}  else {
			gameObject.SetActive(false);
		}
	}

	public void Fire(){
		Player.instance.Fire ();
	}

	public void Aim(){
		Player.instance.toggleZoom ();
	}

}
