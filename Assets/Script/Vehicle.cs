﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Vehicle : WarMachine {
	
	public short acceleration= 1;
	public short maxSpeed = 4;
	public short rotateSpeed = 50;

	protected float currentSpeed = 0f;
}
