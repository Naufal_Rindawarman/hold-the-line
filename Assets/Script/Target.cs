﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Target : MonoBehaviour {

    public Transform target;
	public Transform mainGun;
	private int i = 0;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {		
		//Vector3 q = Vector3.Lerp (transform.position, transform.position-target.position, 0.1f);
		//Vector3 v = Vector3.Lerp(transform.position, target.position, 0.1f);
		//transform.LookAt(v, Vector3.up);
		//transform.rotation = Quaternion.LookRotation (transform.position-target.position);
		Quaternion targetRotation = Quaternion.LookRotation (Vector3.Lerp(transform.forward, target.position-transform.position, 0.05f));
		Quaternion localtargetRotation = Quaternion.LookRotation (Vector3.Lerp(mainGun.forward, target.position-mainGun.position, 0.05f));
		//transform.rotation.SetLookRotation(new Vector3(targetRotation.eulerAngles.x,  targetRotation.eulerAngles.y, targetRotation.eulerAngles.z));
		//transform.rotation = targetRotation;
		transform.rotation = Quaternion.Euler(transform.eulerAngles.x, localtargetRotation.eulerAngles.y, transform.eulerAngles.z);
		mainGun.rotation = Quaternion.Euler(localtargetRotation.eulerAngles.x, mainGun.rotation.eulerAngles.y, mainGun.rotation.eulerAngles.z);


	}
}
