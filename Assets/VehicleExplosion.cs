﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VehicleExplosion : MonoBehaviour {

	public ParticleSystem explosionParticle;
	public ParticleSystem flyingParticle;

	// Use this for initialization
	void OnEnable () {
		EmitParticle ();
	}


	void EmitParticle(){
		explosionParticle.Emit (10);
		flyingParticle.Emit (5);
	}
}
