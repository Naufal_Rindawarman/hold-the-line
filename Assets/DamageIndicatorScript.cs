﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DamageIndicatorScript : MonoBehaviour {

	public Image damageIndicatorPrefab;

	public Transform target;

	public static DamageIndicatorScript instance;

	private Image[] damageindicators;
	private static int index = 0;

	// Use this for initialization
	void Awake(){
		if (instance == null) {
			instance = this;
		} else {
			Destroy (this);
		}
	}

	void Start () {
		index = 0;
		damageindicators = new Image[10];
		damageindicators [0] = damageIndicatorPrefab;
		damageindicators [0].gameObject.SetActive (false);
		for (int i = 1; i < damageindicators.Length; i++) {
			damageindicators [i] = Instantiate (damageIndicatorPrefab.gameObject, transform).GetComponent<Image> ();
			damageindicators [i].gameObject.SetActive (false);
		}

		StartCoroutine(ShowDamageIndicator(target));
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	IEnumerator ShowDamageIndicator(Transform origin){
		float duration = 0f;
		Image currentElement = damageindicators [index];
		if (index >= damageindicators.Length) {
			index = 0;
		}
		currentElement.gameObject.SetActive (true);
		float angle;
		Quaternion lookTo;
		Color startColor = currentElement.color;
		Vector3 originSameLevel;
		originSameLevel.y = Player.instance.transform.position.y;
		while (startColor.a > 0) {
			originSameLevel = origin.position;
			//originSameLevel.y = Camera.main.transform.position.y;
			//startColor.a -= 0.1f*Time.deltaTime;
			currentElement.color = startColor;
			//angle = Vector3.Angle (Player.instance.defenseGunScript.rotateBase.transform.forward, originSameLevel - Player.instance.defenseGunScript.rotateBase.transform.position);
			lookTo = Quaternion.LookRotation(originSameLevel - Player.instance.GetDefenseGunScript().elevateBase.transform.position);
			Debug.Log (lookTo.eulerAngles.y);
			//currentElement.transform.LookAt  (lookTo);

			duration += Time.deltaTime;
			yield return null;
		}
		currentElement.gameObject.SetActive (false);
	}
}
