﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class WarmachineData : ScriptableObject {
	public int maxHealth;
	public string vehicleName;

	public int damage;
	public int ammoCapacity;
	public float reloadTime;
	public int fireRate;

}
