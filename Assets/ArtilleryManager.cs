﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArtilleryManager : MonoBehaviour {

	public static ArtilleryManager instance;
	public int damage = 500;
	public float minMultiplier = 0.2f;
	public float maxMultiplier = 1f;
	public int accuracy = 70;
	public int shellCount = 10;
	public float delay = 1.5f;
	private Vector3 initialPosition;

	public Projectile artilleryProjectile;

	private bool isArtilleryActive = false;

	// Use this for initialization
	void Awake () {
		if (instance == null) {
			instance = this;
		} else {
			Destroy (this);
		}
	}

	void Start(){
		if (minMultiplier > maxMultiplier) {
			maxMultiplier = minMultiplier;
		}
		//artilleryProjectile.SetOwner(Player.instance.GetDefenseGunScript());
		initialPosition = transform.position;
	}

	public bool IsArtilleryActive(){
		return isArtilleryActive;
	}


	public bool CallArtillery(){
		if (!isArtilleryActive) {
			StartCoroutine (CallArty ());
			return true;
		} 
		return false;
	}


	IEnumerator CallArty(){
		int c = 0;
		isArtilleryActive = true;
		WarMachine wm;
		WarMachine target = null;
		yield return new WaitForSeconds(3f);
		while (c < shellCount) {
			c++;

			int isHit = Random.Range (0, 101);
			target = null;
			if (isHit < accuracy) {
				for (int i = 0; i < LevelManager.units.Count; i++) {		
					wm = LevelManager.units [i];
					if (wm.tag == "GroundVehicle" && wm.team == Team.Enemy) {

						int change = Random.Range (0, 10);
						if (change > 6 || target == null) {
							target = wm;
						}
					}
				}
			}

			if (target != null) {
				artilleryProjectile.transform.position = target.transform.position;
				artilleryProjectile.transform.Translate (Vector3.up * 100, Space.World);
				artilleryProjectile.transform.LookAt (target.transform);
			} else {
				artilleryProjectile.transform.position = initialPosition + new Vector3(Random.Range(-75,75),0,Random.Range(-75,50));
			}


			artilleryProjectile.damage = (int) (Random.Range(minMultiplier, maxMultiplier)*damage);
			artilleryProjectile.FireWithoutFlash ();

			yield return new WaitForSeconds(delay* Random.Range(0.8f, 1.2f));
		}
		isArtilleryActive = false;
	}
}
