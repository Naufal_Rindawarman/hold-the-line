﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameMenuManager : MonoBehaviour {

	public static GameMenuManager instance;

	public GameObject endMissionPanel;

	void Awake () {
		if (instance == null) {
			instance = this;
			endMissionPanel.SetActive (false);
		} else {
			Destroy (this);
		}
	}

}
