﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class ButtonClick : MonoBehaviour {

	private AudioSource audio;

	// Use this for initialization
	void Awake () {
		audio = GetComponent<AudioSource> ();
	}

	public void PlayClick(){
		audio.Play ();
	}
	

}
