﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class UIManager : MonoBehaviour {

    // Asign UIManager on Awake, Destroy if already exists
    // Get GameManager and assign to __GM
    void Awake() {

        if (instance == null) {
            instance = this;
			__GM = Object.FindObjectOfType<GameManager>();
        } else {
            DestroyImmediate(this);
        }     

    }

	// UIManager instance
    private static UIManager instance;
    // Get intance, instantiate if null
    public static UIManager Instance {

        get {
            if (instance == null) {
                instance = new UIManager();
            }
            return instance;
        }
    }

    // Constructor
    protected UIManager() { }

    // Get GameManager
    private GameManager __GM;

    // UI
    public Text GoldText;
    public Text PlayerText;
    public RectTransform MainMenu;
    public RectTransform SettingsMenu;
    public RectTransform AboutMenu;
	public RectTransform CampaignSelectMenu;
	public RectTransform LevelSelectMenu;
	public RectTransform GunSelectMenu;

	// Set initial text
    void Start() {

		UpdateGoldText();
        SetPlayerName("MrSegar");
		HideAllMenu ();
		if (GameManager.Instance.firstMenu == FirstMenu.MainMenu) {
			MainMenu.gameObject.SetActive (true);
		} else if (GameManager.Instance.firstMenu == FirstMenu.LevelSelect) {
			LevelSelectMenu.gameObject.SetActive (true);
		} else {
			GunSelectMenu.gameObject.SetActive (true);
		}

    }


    // Set Gold (default or 1 variable)
    public void SetGold() {

        __GM.GOLD = 0;
        UpdateGoldText();
    }

    public void SetGold(float value) {

        __GM.GOLD = value;
        UpdateGoldText();
    }

    // Update Gold Text
    public void UpdateGoldText() {

        GoldText.text = __GM.GOLD.ToString("n0");
    }

    // Set Gold (default or 1 variable)
    public void SetPlayerName() {

        __GM.USERNAME = "John Smith";
        UpdatePlayerText();
    }

    public void SetPlayerName(string value) {

        __GM.USERNAME = value;
        UpdatePlayerText();
    }

    // Update Gold Text
    public void UpdatePlayerText() {

        PlayerText.text = __GM.USERNAME.ToString();
    }

	public void HideAllMenu(){
		MainMenu.gameObject.SetActive(false);
		SettingsMenu.gameObject.SetActive(false);
		AboutMenu.gameObject.SetActive(false);
		CampaignSelectMenu.gameObject.SetActive(false);
		LevelSelectMenu.gameObject.SetActive(false);
		GunSelectMenu.gameObject.SetActive(false);
	}

    // Show Main Menu
    public void ShowMainMenu() {

        MainMenu.gameObject.SetActive(true);
    }

    // Hide Main Menu
    public void HideMainMenu() {

        MainMenu.gameObject.SetActive(false);
    }

    // Show Settings
    public void ShowSettings() {

        SettingsMenu.gameObject.SetActive(true);
    }

    // Hide Settings
    public void HideSettings() {

        SettingsMenu.gameObject.SetActive(false);
    }

    // Show About
    public void ShowAbout() {

        AboutMenu.gameObject.SetActive(true);
    }

    // Hide About
    public void HideAbout() {

        AboutMenu.gameObject.SetActive(false);
    }

    /**
     *  Main Menu Button Handlers
     * */

    public void ButtonCampaignMode() {

    }

    public void ButtonEndlessMode() {

        SceneManager.LoadScene("Plains_Summer");
    }

    public void ButtonArmoryMenu() {

    }

    public void ButtonSettingsMenu() {
        ShowSettings();
    }


    public void ButtonAboutMenu() {
        ShowAbout();
    }

    public void ButtonExitGame() {

        Application.Quit();
        Debug.Log("Game has exited.");
    }

    /**
     *  Settings Menu Button Handlers
     * */

    public void ButtonSettingsBack() {

        HideSettings();
    }

    /**
     *  About Menu Button Handlers
     * */

    public void ButtonAboutBack() {

        HideAbout();
    }
}

public enum FirstMenu {MainMenu, GunSelect, LevelSelect};
