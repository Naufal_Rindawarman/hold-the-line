﻿using UnityEngine;

public class GameManager : MonoBehaviour {

    // Asign GameManager on Awake, Destroy if already exists
    void Awake() {

        if (instance == null) {
            instance = this;
			DontDestroyOnLoad(gameObject);
        } else {
            DestroyImmediate(this);
        }
        
    }

    // UIManager instance
    private static GameManager instance;
    // Get intance, instantiate if null
    public static GameManager Instance {

        get {
            if (instance == null) {
				instance = new GameObject ("_GameManager").AddComponent<GameManager> ();
            }
            return instance;
        }
    }

    // Constructor
    protected GameManager() { }

    // Global Variables
    [SerializeField] public float __GOLD = 0;
    [SerializeField] public string __USERNAME = "";

	public FirstMenu firstMenu = FirstMenu.MainMenu;
	public int _selectedLevel = -99;


    // Getters & Setters
    public float GOLD {
        get {
            return __GOLD;
        }

        set {
            __GOLD = value;
        }
    }

    public string USERNAME {
        get {
            return __USERNAME;
        }

        set {
            __USERNAME = value;
        }
    }

	public int selectedLevel{
		get {
			if (_selectedLevel == -99) {
				_selectedLevel = UnityEngine.SceneManagement.SceneManager.GetActiveScene ().buildIndex;
			}
			return _selectedLevel;
		}

		set  {
			_selectedLevel = value;
		}
	}
}